# IOCBIO WebDB

This is a web framework that can be used for development of
site-specific applications allowing you to fill enter the data into
the database and query it. The code has been developed for scientific
laboratory use to enter data describing experiments, solutions, chemicals
and similar. It is a part of the set of tools that are targeting handling
of experimental data flow through database backend.

As it is one of the tools that is used to access and modify the data,
creation of the database tables and user's access is expected to be
configured on database side. In practice, users will have to be able
to login into the database with their user credentials.

Interaction with the database is defined through SQL statements,
definition of Python classes describing the tables and interaction
between table fields. Web site structure is defined through routing
table.

Demo application is included in this repository, in `demo` branch. How
to set it up and test is covered below in a separate subsection of
this README. In addition, an example application used in practice in
[Laboratory of Systems Biology, Department of Cybernetics, Tallinn
University of Technology](https://sysbio.ioc.ee) is provided as a
separate branch (sysbio) of this repository.

The application is based on the following software:

- [Pyramid](https://trypyramid.com/) - web framework
- [records.py](https://github.com/kennethreitz/records) - database access through SQL statements
- [SQLAlchemy](https://www.sqlalchemy.org/) - database library used by records.py
- [psycopg2](http://initd.org/psycopg/) - database library used by SQLAlchemy
- [skeleton.css](http://getskeleton.com) - responsive CSS boilerplate
- [Jinja2](https://jinja.palletsprojects.com) - templating language for Python
- JavaScript libraries: [jQuery](https://jquery.com/), [pickadate.js](https://amsul.ca/pickadate.js/),
[jQuery Timepicker](https://timepicker.co/), and [selectize.js](https://selectize.github.io/selectize.js/)


## Database user and access model

To use this application, a database user has to be created which is
allowed to connect to the database. Only connection to the database is
required as a permission. This user should not inherit any privileges
from the groups it belongs to. However, this user should be a member
of every user that is allowed to use this application. As a result,
the database user can change its role to the logged in user for every
transaction of the user and use only the privileges given to that user.

As the access model requires user/password authentication, databases
without it, such as SQLite, are not supported.


## Usage by end-users

The resulting application is formed around database tables, queries in
them and ability to add or edit the data. In addition, it is possible
to further select elements in the queries of the data presented to the
user using search function. For that, search string is parsed to
identify parts corresponding to query parts which will apply for some
specific field and which will apply as free text search.

Syntax of the search field has a form `key=value key2<value2 free
search "text search 2"` where examples showing `key`, `value` and the
operator between them are shown as well as ability to search for lines
in the table with strings `free`, `search`, and `text search 2`. Few
notes:

- `key` should have a column with such name in SQL query used to
  display the table.
  
- supported operators are `>=`, `<=`, `!=`, `<>`, `>`, `<`, `=`. Out
  of these, only `!=`, `<>`, `=` are supported for the columns that
  have array type values.

- there should be no space between `key=value` pair (or any other
  operator).

- if you need to use a string with a space, use the quotes to make it
  recognized in full, as in `"text string"`.

- to filter by checking whether column value is or is not NULL, use
  `NULL` as a value.

When user has restricted the results using `key=value` pairs and
wishes to add new items into the table, this restriction will be
applied in the entry forms as well. This allows to easily enter data
with the similar restrictions. All restrictions can be lifted by
editing the search string on the table view.


## Setting up web application

You will need to have created the database, tables, and users. The
application is written using PostgreSQL as a backend and has not been
tested against any other database engine. We use Apache as a web
server, [phpPgAdmin](http://phppgadmin.sourceforge.net) for database
administration, and Apache's connection to the application through
mod_wsgi, as described in a
[tutorial](https://docs.pylonsproject.org/projects/pyramid/en/latest/tutorials/modwsgi/).

For application, create `development.ini` or `production.ini` on the
basis of included templates. Define `main_title`, `database_template`,
`database_user`, and `database_password`.


## How to write your own

To write your own application, you need to know the basics of Pyramid
(routes and views), SQL to make queries, and Jinja2 to display data if
the provided views for tables are insufficient. For examples, see
`demo` and `sysbio` branches.

For data, we use `TableView` as the main class from which all other
tables are derived. See demos in separate branches.

To write your own application, we would suggest to fork this
repository and add functionality to `webdb/site/` subfolder. This
folder will not be changed in this repository so you could put all
your changes there. If some other changes are needed in other parts of
the code, consider contributing them back to this repository.

For CSS changes, you can extend the used Skeleton at
[webdb/static/css/this_site.css](webdb/static/css/this_site.css). Alternative
would be to change CSS and load it in
[webdb/templates/layout.jinja2](webdb/templates/layout.jinja2) instead
of Skeleton.

To adjust your README, replace README.md with your text. It is
expected that upstream README.md will be not changed or changed very
rarely as it is partitioned into the main text and the header. That
will allow you to merge with the upstream later with all upstream
README changes going into README.webdb.md.


## Installing and testing application

- Create a Python virtual environment.

    python3 -m venv env

- Upgrade packaging tools.

    env/bin/pip install --upgrade pip setuptools

- Install the project in editable mode with its testing requirements.

    env/bin/pip install -e ".[testing]"

- Run your project's tests.

    env/bin/pytest

- Run your project.

    env/bin/pserve development.ini


## Demo

Demo application is in separate branch `demo`. To initialize it, clone
this repository and change the branch to demo.


### Setup database

For a start, setup database if you don't have any. PostgreSQL is
recommended as it is the one the software is developed for. See
installation instructions for your platform. The database has to be
using user/password authentication.

### Setup database users

You will need at least two users:

- webdb (or something else used by the app)
- database_user, you

Here, `webdb` user should be a member of `database_user` (see access
model above) and `database_user` would be the one that has
permissions. Note, for security, `webdb` should NOT inherit
privileges.

### Setup database tables

Schema is in
[`misc/schema_chemicals.sql`](misc/schema_chemicals.sql). Apply it
using, after adjusting `psql` command arguments to list the correct
database and add user name if needed:

```
psql testdb < misc/schema_chemicals.sql
```


### Setup login data in development.ini

Make a copy of [`development.ini.template`](development.ini.template)
as `development.ini`:

```
cp development.ini.template development.ini
```

Then edit `development.ini` to include you database connection details
for `webdb` used (or any other dedicated user that you made for
database connection). For example, as in

```
database_template = postgresql://{0}:{1}@localhost/testdb
database_user = webdb
database_password = webdb
```


### Start the application

Now you should be able to start the application:

```
env/bin/pserve development.ini
```

This will print out URL used to serve the page. You can connect to it
via your web browser. You will be pointed to login page where you
would have to use `database_user` (you) login data. This will allow
the application to authorize you and interact on your behalf in the
communication with the database.

After successful login, you should see the application homepage with
the tables describing chemicals. To use, define new records in the following order:

* chemical - general name and data of the chemical. Example: NaCl
* location - location in the lab. Example: Blue box
* company - company producing the chemicals and another reselling it
* lot - specific chemical lot, linking chemical, location, manufacturer and reseller
* solution - define solution name and then proceed to define components using other solutions and chemical lots.
