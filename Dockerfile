FROM ubuntu:latest

RUN apt-get update -y
RUN apt-get install -y python3-pip python3-dev
RUN pip3 install --upgrade pip setuptools

WORKDIR /app
COPY requirements.txt /app/requirements.txt
COPY setup.py README.md /app/
COPY webdb /app/webdb

RUN pip3 install -r requirements.txt
RUN pip3 install .

EXPOSE 6543
