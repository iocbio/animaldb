import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()

requires = [
    'plaster_pastedeploy',
    'pyramid',
    'pyramid_jinja2',
    'pyramid_debugtoolbar',
    'waitress',
    'records',
    'psycopg2-binary',
    'SQLAlchemy',
    'python-dateutil',
    'attrdict3'
]

tests_require = [
    'WebTest >= 1.3.1',  # py3 compat
    'pytest >= 3.7.4',
    'pytest-cov',
]

setup(
    name='webdb',
    version='0.0',
    description='IOCBIO WebDB: Database Web App Framework',
    long_description=README,
    long_description_content_type="text/markdown",
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Intended Audience :: Science/Research',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    author='IOCBio team',
    author_email='iocbio@sysbio.ioc.ee',
    license='GPLv3',
    url='https://gitlab.com/iocbio/webdb',
    keywords='web pyramid pylons postgresql',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
    },
    install_requires=requires,
    entry_points={
        'paste.app_factory': [
            'main = webdb:main',
        ],
    },
)
