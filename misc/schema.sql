--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: animals; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA animals;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: animal; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.animal (
    id integer NOT NULL,
    sex character(1) DEFAULT 'F'::bpchar NOT NULL,
    litter integer NOT NULL,
    death date,
    export integer,
    comment character varying,
    death_reason integer,
    death_permission integer,
    CONSTRAINT animal_death_check CHECK ((((death IS NOT NULL) AND (death_reason IS NOT NULL) AND (death_permission IS NOT NULL)) OR ((death IS NULL) AND (death_reason IS NULL)))),
    CONSTRAINT animal_sex_check CHECK (((sex = 'F'::bpchar) OR (sex = 'M'::bpchar)))
);


--
-- Name: TABLE animal; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.animal IS 'Individual animals';


--
-- Name: COLUMN animal.sex; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal.sex IS 'Animal sex, ''F'' for female, ''M'' for male';


--
-- Name: COLUMN animal.death; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal.death IS 'Date of death, set to NULL if exported';


--
-- Name: COLUMN animal.export; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal.export IS 'Reference to export, set to NULL if used in animal facility';


--
-- Name: animal2cage; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.animal2cage (
    id integer NOT NULL,
    animal integer NOT NULL,
    cage integer,
    "when" timestamp without time zone DEFAULT (now())::timestamp(0) with time zone NOT NULL,
    comment character varying,
    "user" integer NOT NULL
);


--
-- Name: TABLE animal2cage; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.animal2cage IS 'Movement of animals between the cages';


--
-- Name: COLUMN animal2cage.animal; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal2cage.animal IS 'Animal moved between cages';


--
-- Name: COLUMN animal2cage.cage; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal2cage.cage IS 'Cage to which the animal is moved to';


--
-- Name: COLUMN animal2cage."when"; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal2cage."when" IS 'Date and time at which the movement was performed';


--
-- Name: COLUMN animal2cage."user"; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal2cage."user" IS 'Who performed the movement';


--
-- Name: animal2cage_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.animal2cage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: animal2cage_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.animal2cage_id_seq OWNED BY animals.animal2cage.id;


--
-- Name: animal2cage_last; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal2cage_last AS
 SELECT DISTINCT ON (animal2cage.animal) animal2cage.id,
    animal2cage.animal,
    animal2cage.cage,
    animal2cage."when",
    animal2cage."user",
    animal2cage.comment
   FROM animals.animal2cage
  ORDER BY animal2cage.animal, animal2cage."when" DESC;


--
-- Name: export; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.export (
    id integer NOT NULL,
    name character varying NOT NULL,
    departure date NOT NULL,
    comment character varying
);


--
-- Name: TABLE export; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.export IS 'Export of animals from facility';


--
-- Name: litter; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.litter (
    id integer NOT NULL,
    mating integer,
    birth date DEFAULT (now())::timestamp(0) with time zone NOT NULL,
    comment character varying,
    import integer,
    CONSTRAINT litter_check_import_mating_check CHECK ((((import IS NULL) AND (mating IS NOT NULL)) OR ((import IS NOT NULL) AND (mating IS NULL))))
);


--
-- Name: TABLE litter; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.litter IS 'Animal litters';


--
-- Name: COLUMN litter.mating; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.litter.mating IS 'Reference to mating ID, set to NULL for imports';


--
-- Name: COLUMN litter.birth; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.litter.birth IS 'Date of birth';


--
-- Name: animal_age; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal_age AS
 SELECT am.id,
    (((date_part('epoch'::text,
        CASE
            WHEN (am.death IS NOT NULL) THEN age((am.death)::timestamp with time zone, (aml.birth)::timestamp with time zone)
            WHEN (ex.departure IS NOT NULL) THEN age((ex.departure)::timestamp with time zone, (aml.birth)::timestamp with time zone)
            ELSE age((aml.birth)::timestamp with time zone)
        END) / (24)::double precision) / (60)::double precision) / (60)::double precision) AS age_in_days
   FROM ((animals.animal am
     JOIN animals.litter aml ON ((am.litter = aml.id)))
     LEFT JOIN animals.export ex ON ((ex.id = am.export)));


--
-- Name: animal_cost; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.animal_cost (
    id integer NOT NULL,
    animal integer NOT NULL,
    cost_account integer NOT NULL,
    "from" date,
    till date,
    comment character varying
);


--
-- Name: TABLE animal_cost; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.animal_cost IS 'Link between animals and cost accounts';


--
-- Name: COLUMN animal_cost.animal; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal_cost.animal IS 'Animal';


--
-- Name: COLUMN animal_cost.cost_account; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal_cost.cost_account IS 'Cost account ';


--
-- Name: COLUMN animal_cost."from"; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal_cost."from" IS 'Date from which cost associated with the animal is booked on account. Leave NULL if from the animal birth';


--
-- Name: COLUMN animal_cost.till; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.animal_cost.till IS 'Date till which cost associated with the animal is booked on account. Leave NULL if till death or export';


--
-- Name: animal_cost_timespan; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal_cost_timespan AS
 SELECT ac.id,
        CASE
            WHEN ((ac."from" IS NULL) OR (ac."from" < l.birth)) THEN l.birth
            ELSE ac."from"
        END AS "from",
        CASE
            WHEN ((ac.till IS NULL) AND (am.death IS NULL) AND (ex.departure IS NULL)) THEN (now())::date
            WHEN ((ac.till IS NULL) OR ((am.death IS NOT NULL) AND (ac.till > am.death)) OR ((ex.departure IS NOT NULL) AND (ac.till > ex.departure))) THEN COALESCE(am.death, ex.departure)
            ELSE ac.till
        END AS till
   FROM (((animals.animal_cost ac
     JOIN animals.animal am ON ((ac.animal = am.id)))
     JOIN animals.litter l ON ((l.id = am.litter)))
     LEFT JOIN animals.export ex ON ((am.export = ex.id)));


--
-- Name: VIEW animal_cost_timespan; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON VIEW animals.animal_cost_timespan IS 'Animal cost records with filled dates for FROM and TILL fields. Use together with a join on animal_cost table by joining using its IDs';


--
-- Name: animal_property; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.animal_property (
    id integer NOT NULL,
    animal integer NOT NULL,
    property integer NOT NULL
);


--
-- Name: TABLE animal_property; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.animal_property IS 'Collection of animal properties';


--
-- Name: property; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.property (
    id integer NOT NULL,
    name character varying NOT NULL,
    inherited boolean DEFAULT false NOT NULL,
    comment character varying,
    sorting_order integer DEFAULT 100 NOT NULL,
    cost boolean DEFAULT false NOT NULL
);


--
-- Name: TABLE property; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.property IS 'Animal properties';


--
-- Name: COLUMN property.inherited; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.property.inherited IS 'If inherited and all parents of the litter have the same property, it will be applied to the litter animals as well';


--
-- Name: COLUMN property.sorting_order; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.property.sorting_order IS 'Sorting order of the property';


--
-- Name: COLUMN property.cost; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.property.cost IS 'Is this property associated with the cost per day';


--
-- Name: animal_cost_accumulated_days; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal_cost_accumulated_days AS
 SELECT ac.id,
    ac.animal,
    ac.cost_account,
    p.id AS property,
    p.name AS property_name,
    (t.till - t."from") AS days
   FROM (((animals.animal_cost ac
     JOIN animals.animal_cost_timespan t ON ((t.id = ac.id)))
     JOIN animals.animal_property ap ON ((ac.animal = ap.animal)))
     JOIN animals.property p ON ((ap.property = p.id)))
  WHERE p.cost;


--
-- Name: animal_cost_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.animal_cost_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: animal_cost_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.animal_cost_id_seq OWNED BY animals.animal_cost.id;


--
-- Name: animal_cost_last; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal_cost_last AS
 SELECT DISTINCT ON (ac.animal) ac.id,
    ac.animal,
    ac.cost_account,
    t."from",
    t.till,
    ac.comment
   FROM (animals.animal_cost ac
     JOIN animals.animal_cost_timespan t ON ((ac.id = t.id)))
  ORDER BY ac.animal, ac.till DESC;


--
-- Name: animal_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.animal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: animal_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.animal_id_seq OWNED BY animals.animal.id;


--
-- Name: animal_in_facility; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal_in_facility AS
 SELECT am.id
   FROM ((animals.animal am
     JOIN animals.litter l ON ((am.litter = l.id)))
     LEFT JOIN animals.export e ON ((am.export = e.id)))
  WHERE ((l.birth < now()) AND ((am.death IS NULL) OR (am.death > now())) AND ((am.export IS NULL) OR (e.departure > now())));


--
-- Name: animal_property_array; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal_property_array AS
 SELECT array_agg(grouped.name) AS properties,
    grouped.animal
   FROM ( SELECT p.name,
            ap.animal
           FROM (animals.animal_property ap
             JOIN animals.property p ON ((p.id = ap.property)))
          ORDER BY ap.animal, p.sorting_order, p.name) grouped
  GROUP BY grouped.animal;


--
-- Name: animal_name; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.animal_name AS
 SELECT am.id,
    concat((am.id)::text, ' / ', am.sex, ' / ', array_to_string(ap.properties, ' '::text), ' / ', ag.age_in_days, ' days') AS name
   FROM ((animals.animal am
     LEFT JOIN animals.animal_property_array ap ON ((ap.animal = am.id)))
     JOIN animals.animal_age ag ON ((am.id = ag.id)));


--
-- Name: animal_property_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.animal_property_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: animal_property_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.animal_property_id_seq OWNED BY animals.animal_property.id;


--
-- Name: cage; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.cage (
    id integer NOT NULL,
    name character varying NOT NULL,
    comment character varying
);


--
-- Name: TABLE cage; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.cage IS 'Cages in the Facility';


--
-- Name: COLUMN cage.name; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.cage.name IS 'Identifiable name, as written on the cage';


--
-- Name: cage2location; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.cage2location (
    id integer NOT NULL,
    cage integer NOT NULL,
    location integer NOT NULL,
    "when" timestamp without time zone DEFAULT (now())::timestamp(0) with time zone NOT NULL,
    comment character varying,
    "user" integer NOT NULL
);


--
-- Name: TABLE cage2location; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.cage2location IS 'Location of cages';


--
-- Name: COLUMN cage2location."user"; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.cage2location."user" IS 'Who performed the movement';


--
-- Name: cage2location_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.cage2location_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cage2location_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.cage2location_id_seq OWNED BY animals.cage2location.id;


--
-- Name: cage2location_last; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.cage2location_last AS
 SELECT DISTINCT ON (cage2location.cage) cage2location.id,
    cage2location.cage,
    cage2location.location,
    cage2location."when",
    cage2location."user",
    cage2location.comment
   FROM animals.cage2location
  ORDER BY cage2location.cage, cage2location."when" DESC;


--
-- Name: cage_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.cage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cage_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.cage_id_seq OWNED BY animals.cage.id;


--
-- Name: cost_account; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.cost_account (
    id integer NOT NULL,
    name character varying NOT NULL,
    comment character varying,
    active boolean DEFAULT true NOT NULL,
    "user" integer NOT NULL
);


--
-- Name: TABLE cost_account; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.cost_account IS 'Cost accounts';


--
-- Name: COLUMN cost_account.name; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.cost_account.name IS 'Name of the account';


--
-- Name: COLUMN cost_account."user"; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.cost_account."user" IS 'Contact user';


--
-- Name: cost_account_costs_accumulated; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.cost_account_costs_accumulated AS
 SELECT c.id,
    ac.property,
    sum(ac.days) AS amount
   FROM (animals.cost_account c
     JOIN animals.animal_cost_accumulated_days ac ON ((c.id = ac.cost_account)))
  GROUP BY c.id, ac.property;


--
-- Name: invoice; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.invoice (
    id integer NOT NULL,
    "when" timestamp without time zone DEFAULT (now())::timestamp(0) with time zone NOT NULL,
    cost_account integer NOT NULL,
    amount real NOT NULL,
    comment character varying,
    property integer NOT NULL
);


--
-- Name: TABLE invoice; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.invoice IS 'Invoices presented for the costs';


--
-- Name: COLUMN invoice."when"; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.invoice."when" IS 'Time when invoice was presented';


--
-- Name: COLUMN invoice.cost_account; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.invoice.cost_account IS 'Associated cost account';


--
-- Name: COLUMN invoice.amount; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals.invoice.amount IS 'Invoice amount';


--
-- Name: cost_account_costs_next_invoice; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.cost_account_costs_next_invoice AS
 SELECT ac.id,
    ac.property,
    now() AS "timestamp",
    ((ac.amount)::double precision - COALESCE(inv.invoiced, (0)::real)) AS amount
   FROM (animals.cost_account_costs_accumulated ac
     LEFT JOIN ( SELECT inv_1.cost_account,
            inv_1.property,
            sum(inv_1.amount) AS invoiced
           FROM animals.invoice inv_1
          GROUP BY inv_1.cost_account, inv_1.property) inv ON (((ac.id = inv.cost_account) AND (ac.property = inv.property))));


--
-- Name: cost_account_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.cost_account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cost_account_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.cost_account_id_seq OWNED BY animals.cost_account.id;


--
-- Name: death_reason; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.death_reason (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


--
-- Name: TABLE death_reason; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.death_reason IS 'Types of death reasons';


--
-- Name: death_reason_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.death_reason_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: death_reason_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.death_reason_id_seq OWNED BY animals.death_reason.id;


--
-- Name: earmark; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.earmark (
    id integer NOT NULL,
    animal integer NOT NULL,
    earmark integer NOT NULL
);


--
-- Name: TABLE earmark; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.earmark IS 'Earmark of the animal';


--
-- Name: earmark_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.earmark_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: earmark_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.earmark_id_seq OWNED BY animals.earmark.id;


--
-- Name: experiment; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.experiment (
    id integer NOT NULL,
    experiment_type integer NOT NULL,
    permission integer NOT NULL,
    "from" timestamp without time zone NOT NULL,
    till timestamp without time zone NOT NULL,
    comment character varying,
    animal integer NOT NULL
);


--
-- Name: TABLE experiment; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.experiment IS 'Experiments performed on animals';


--
-- Name: experiment_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.experiment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: experiment_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.experiment_id_seq OWNED BY animals.experiment.id;


--
-- Name: experiment_type; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.experiment_type (
    id integer NOT NULL,
    name character varying NOT NULL,
    comment character varying
);


--
-- Name: TABLE experiment_type; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.experiment_type IS 'Types of experiments';


--
-- Name: experiment_type_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.experiment_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: experiment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.experiment_type_id_seq OWNED BY animals.experiment_type.id;


--
-- Name: export_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.export_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: export_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.export_id_seq OWNED BY animals.export.id;


--
-- Name: import; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.import (
    id integer NOT NULL,
    name character varying,
    arrival date NOT NULL,
    comment character varying
);


--
-- Name: TABLE import; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.import IS 'Imports of animals into the facility';


--
-- Name: import_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.import_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: import_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.import_id_seq OWNED BY animals.import.id;


--
-- Name: invoice_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.invoice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.invoice_id_seq OWNED BY animals.invoice.id;


--
-- Name: litter_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.litter_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: litter_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.litter_id_seq OWNED BY animals.litter.id;


--
-- Name: location; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.location (
    id integer NOT NULL,
    name character varying NOT NULL,
    comment character varying
);


--
-- Name: TABLE location; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.location IS 'Locations in the facility';


--
-- Name: location_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.location_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.location_id_seq OWNED BY animals.location.id;


--
-- Name: mating; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.mating (
    id integer NOT NULL,
    comment character varying,
    success boolean
);


--
-- Name: TABLE mating; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.mating IS 'Breeding';


--
-- Name: mating_parent; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.mating_parent (
    mating integer NOT NULL,
    parent integer NOT NULL,
    "from" date DEFAULT (now())::timestamp(0) with time zone NOT NULL,
    till date,
    comment character varying,
    id integer NOT NULL
);


--
-- Name: TABLE mating_parent; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.mating_parent IS 'List of parents';


--
-- Name: mating_cost_account; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.mating_cost_account AS
 SELECT m.mating,
    mc.cost_account
   FROM (( SELECT mating_parent.mating,
            count(*) AS parent_count
           FROM animals.mating_parent
          GROUP BY mating_parent.mating) m
     JOIN ( SELECT mp.mating,
            acl.cost_account,
            count(*) AS cost_account_count
           FROM (animals.mating_parent mp
             JOIN animals.animal_cost_last acl ON ((mp.parent = acl.animal)))
          GROUP BY mp.mating, acl.cost_account) mc ON ((m.mating = mc.mating)))
  WHERE (m.parent_count = mc.cost_account_count);


--
-- Name: mating_death_permission; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.mating_death_permission AS
 SELECT m.mating,
    mp.death_permission
   FROM (( SELECT mating_parent.mating,
            count(*) AS parent_count
           FROM animals.mating_parent
          GROUP BY mating_parent.mating) m
     JOIN ( SELECT mp_1.mating,
            am.death_permission,
            count(*) AS permission_count
           FROM (animals.mating_parent mp_1
             JOIN animals.animal am ON ((mp_1.parent = am.id)))
          GROUP BY mp_1.mating, am.death_permission) mp ON ((m.mating = mp.mating)))
  WHERE (m.parent_count = mp.permission_count);


--
-- Name: mating_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.mating_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mating_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.mating_id_seq OWNED BY animals.mating.id;


--
-- Name: mating_property; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.mating_property AS
 SELECT m.mating,
    mp.property
   FROM (( SELECT mating_parent.mating,
            count(*) AS parent_count
           FROM animals.mating_parent
          GROUP BY mating_parent.mating) m
     JOIN ( SELECT mp_1.mating,
            ap.property,
            count(*) AS prop_count
           FROM ((animals.mating_parent mp_1
             JOIN animals.animal_property ap ON ((ap.animal = mp_1.parent)))
             JOIN animals.property p ON ((ap.property = p.id)))
          WHERE p.inherited
          GROUP BY mp_1.mating, ap.property) mp ON ((m.mating = mp.mating)))
  WHERE (m.parent_count = mp.prop_count);


--
-- Name: VIEW mating_property; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON VIEW animals.mating_property IS 'Inherited properties from all parents involved in mating';


--
-- Name: mating_inherited_overview; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.mating_inherited_overview AS
 SELECT m.id AS mating,
    mp.properties,
    mp.properties_names,
    adp.death_permission,
    mca.cost_account
   FROM (((animals.mating m
     LEFT JOIN ( SELECT mp_1.mating,
            array_agg(mp_1.property) AS properties,
            array_agg(mp_1.property_name) AS properties_names
           FROM ( SELECT mp_2.mating,
                    p.id AS property,
                    p.name AS property_name
                   FROM (animals.mating_property mp_2
                     JOIN animals.property p ON ((mp_2.property = p.id)))
                  ORDER BY p.sorting_order, p.name) mp_1
          GROUP BY mp_1.mating) mp ON ((mp.mating = m.id)))
     LEFT JOIN animals.mating_cost_account mca ON ((mca.mating = m.id)))
     LEFT JOIN animals.mating_death_permission adp ON ((adp.mating = m.id)));


--
-- Name: mating_overview; Type: VIEW; Schema: animals; Owner: -
--

CREATE VIEW animals.mating_overview AS
 SELECT m.id,
    timespan."from",
    timespan.till,
    pars_m.parents AS male_parents,
    pars_f.parents AS female_parents
   FROM (((animals.mating m
     LEFT JOIN ( SELECT min(par."from") AS "from",
            max(par.till) AS till,
            par.mating
           FROM animals.mating_parent par
          GROUP BY par.mating) timespan ON ((timespan.mating = m.id)))
     LEFT JOIN ( SELECT string_agg((am.id)::text, ','::text) AS parents,
            par.mating
           FROM (animals.mating_parent par
             JOIN animals.animal am ON ((par.parent = am.id)))
          WHERE (am.sex = 'M'::bpchar)
          GROUP BY par.mating) pars_m ON ((pars_m.mating = m.id)))
     LEFT JOIN ( SELECT string_agg((am.id)::text, ','::text) AS parents,
            par.mating
           FROM (animals.mating_parent par
             JOIN animals.animal am ON ((par.parent = am.id)))
          WHERE (am.sex = 'F'::bpchar)
          GROUP BY par.mating) pars_f ON ((pars_f.mating = m.id)));


--
-- Name: mating_parent_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.mating_parent_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mating_parent_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.mating_parent_id_seq OWNED BY animals.mating_parent.id;


--
-- Name: permission; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals.permission (
    id integer NOT NULL,
    name character varying NOT NULL,
    "user" integer NOT NULL,
    comment character varying,
    "from" date NOT NULL,
    till date NOT NULL
);


--
-- Name: TABLE permission; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals.permission IS 'Animal experiment permissions';


--
-- Name: permission_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permission_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.permission_id_seq OWNED BY animals.permission.id;


--
-- Name: property_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.property_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: property_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.property_id_seq OWNED BY animals.property.id;


--
-- Name: user; Type: TABLE; Schema: animals; Owner: -
--

CREATE TABLE animals."user" (
    id integer NOT NULL,
    name character varying NOT NULL,
    database_user_name character varying,
    phone character varying,
    email character varying
);


--
-- Name: TABLE "user"; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON TABLE animals."user" IS 'Users of the facility';


--
-- Name: COLUMN "user".name; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals."user".name IS 'Legal name of the user';


--
-- Name: COLUMN "user".database_user_name; Type: COMMENT; Schema: animals; Owner: -
--

COMMENT ON COLUMN animals."user".database_user_name IS 'User name in the database, if available';


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: animals; Owner: -
--

CREATE SEQUENCE animals.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: animals; Owner: -
--

ALTER SEQUENCE animals.user_id_seq OWNED BY animals."user".id;


--
-- Name: animal id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal ALTER COLUMN id SET DEFAULT nextval('animals.animal_id_seq'::regclass);


--
-- Name: animal2cage id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal2cage ALTER COLUMN id SET DEFAULT nextval('animals.animal2cage_id_seq'::regclass);


--
-- Name: animal_cost id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_cost ALTER COLUMN id SET DEFAULT nextval('animals.animal_cost_id_seq'::regclass);


--
-- Name: animal_property id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_property ALTER COLUMN id SET DEFAULT nextval('animals.animal_property_id_seq'::regclass);


--
-- Name: cage id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage ALTER COLUMN id SET DEFAULT nextval('animals.cage_id_seq'::regclass);


--
-- Name: cage2location id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage2location ALTER COLUMN id SET DEFAULT nextval('animals.cage2location_id_seq'::regclass);


--
-- Name: cost_account id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cost_account ALTER COLUMN id SET DEFAULT nextval('animals.cost_account_id_seq'::regclass);


--
-- Name: death_reason id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.death_reason ALTER COLUMN id SET DEFAULT nextval('animals.death_reason_id_seq'::regclass);


--
-- Name: earmark id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.earmark ALTER COLUMN id SET DEFAULT nextval('animals.earmark_id_seq'::regclass);


--
-- Name: experiment id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment ALTER COLUMN id SET DEFAULT nextval('animals.experiment_id_seq'::regclass);


--
-- Name: experiment_type id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment_type ALTER COLUMN id SET DEFAULT nextval('animals.experiment_type_id_seq'::regclass);


--
-- Name: export id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.export ALTER COLUMN id SET DEFAULT nextval('animals.export_id_seq'::regclass);


--
-- Name: import id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.import ALTER COLUMN id SET DEFAULT nextval('animals.import_id_seq'::regclass);


--
-- Name: invoice id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.invoice ALTER COLUMN id SET DEFAULT nextval('animals.invoice_id_seq'::regclass);


--
-- Name: litter id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.litter ALTER COLUMN id SET DEFAULT nextval('animals.litter_id_seq'::regclass);


--
-- Name: location id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.location ALTER COLUMN id SET DEFAULT nextval('animals.location_id_seq'::regclass);


--
-- Name: mating id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.mating ALTER COLUMN id SET DEFAULT nextval('animals.mating_id_seq'::regclass);


--
-- Name: mating_parent id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.mating_parent ALTER COLUMN id SET DEFAULT nextval('animals.mating_parent_id_seq'::regclass);


--
-- Name: permission id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.permission ALTER COLUMN id SET DEFAULT nextval('animals.permission_id_seq'::regclass);


--
-- Name: property id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.property ALTER COLUMN id SET DEFAULT nextval('animals.property_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals."user" ALTER COLUMN id SET DEFAULT nextval('animals.user_id_seq'::regclass);


--
-- Name: animal2cage animal2cage_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal2cage
    ADD CONSTRAINT animal2cage_pk PRIMARY KEY (id);


--
-- Name: animal_cost animal_cost_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_cost
    ADD CONSTRAINT animal_cost_pk PRIMARY KEY (id);


--
-- Name: animal animal_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal
    ADD CONSTRAINT animal_pk PRIMARY KEY (id);


--
-- Name: animal_property animal_property_animal_property_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_property
    ADD CONSTRAINT animal_property_animal_property_un UNIQUE (animal, property);


--
-- Name: animal_property animal_property_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_property
    ADD CONSTRAINT animal_property_pk PRIMARY KEY (id);


--
-- Name: cage2location cage2location_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage2location
    ADD CONSTRAINT cage2location_pk PRIMARY KEY (id);


--
-- Name: cage cage_name_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage
    ADD CONSTRAINT cage_name_un UNIQUE (name);


--
-- Name: cage cage_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage
    ADD CONSTRAINT cage_pk PRIMARY KEY (id);


--
-- Name: cost_account cost_accounts_name_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cost_account
    ADD CONSTRAINT cost_accounts_name_un UNIQUE (name);


--
-- Name: cost_account cost_accounts_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cost_account
    ADD CONSTRAINT cost_accounts_pk PRIMARY KEY (id);


--
-- Name: death_reason death_reason_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.death_reason
    ADD CONSTRAINT death_reason_pk PRIMARY KEY (id);


--
-- Name: earmark earmark_animal_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.earmark
    ADD CONSTRAINT earmark_animal_un UNIQUE (animal);


--
-- Name: earmark earmark_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.earmark
    ADD CONSTRAINT earmark_pk PRIMARY KEY (id);


--
-- Name: experiment experiment_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment
    ADD CONSTRAINT experiment_pk PRIMARY KEY (id);


--
-- Name: experiment_type experiment_type_name_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment_type
    ADD CONSTRAINT experiment_type_name_un UNIQUE (name);


--
-- Name: experiment_type experiment_type_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment_type
    ADD CONSTRAINT experiment_type_pk PRIMARY KEY (id);


--
-- Name: export export_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.export
    ADD CONSTRAINT export_pk PRIMARY KEY (id);


--
-- Name: import import_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.import
    ADD CONSTRAINT import_pk PRIMARY KEY (id);


--
-- Name: invoice invoice_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.invoice
    ADD CONSTRAINT invoice_pk PRIMARY KEY (id);


--
-- Name: litter litter_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.litter
    ADD CONSTRAINT litter_pk PRIMARY KEY (id);


--
-- Name: location location_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.location
    ADD CONSTRAINT location_pk PRIMARY KEY (id);


--
-- Name: location location_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.location
    ADD CONSTRAINT location_un UNIQUE (name);


--
-- Name: mating_parent mating_parent_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.mating_parent
    ADD CONSTRAINT mating_parent_pk PRIMARY KEY (id);


--
-- Name: mating mating_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.mating
    ADD CONSTRAINT mating_pk PRIMARY KEY (id);


--
-- Name: permission permission_name_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.permission
    ADD CONSTRAINT permission_name_un UNIQUE (name);


--
-- Name: permission permission_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.permission
    ADD CONSTRAINT permission_pk PRIMARY KEY (id);


--
-- Name: property property_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.property
    ADD CONSTRAINT property_pk PRIMARY KEY (id);


--
-- Name: user user_dbusername_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals."user"
    ADD CONSTRAINT user_dbusername_un UNIQUE (database_user_name);


--
-- Name: user user_name_un; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals."user"
    ADD CONSTRAINT user_name_un UNIQUE (name);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- Name: animal2cage animal2cage_animal_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal2cage
    ADD CONSTRAINT animal2cage_animal_fk FOREIGN KEY (animal) REFERENCES animals.animal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: animal2cage animal2cage_cage_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal2cage
    ADD CONSTRAINT animal2cage_cage_fk FOREIGN KEY (cage) REFERENCES animals.cage(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: animal2cage animal2cage_user_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal2cage
    ADD CONSTRAINT animal2cage_user_fk FOREIGN KEY ("user") REFERENCES animals."user"(id);


--
-- Name: animal_cost animal_cost_animal_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_cost
    ADD CONSTRAINT animal_cost_animal_fk FOREIGN KEY (animal) REFERENCES animals.animal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: animal_cost animal_cost_cost_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_cost
    ADD CONSTRAINT animal_cost_cost_fk FOREIGN KEY (cost_account) REFERENCES animals.cost_account(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: animal animal_death_permission_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal
    ADD CONSTRAINT animal_death_permission_fk FOREIGN KEY (death_permission) REFERENCES animals.permission(id);


--
-- Name: animal animal_death_reason_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal
    ADD CONSTRAINT animal_death_reason_fk FOREIGN KEY (death_reason) REFERENCES animals.death_reason(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: animal animal_export_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal
    ADD CONSTRAINT animal_export_fk FOREIGN KEY (export) REFERENCES animals.export(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: animal animal_litter_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal
    ADD CONSTRAINT animal_litter_fk FOREIGN KEY (litter) REFERENCES animals.litter(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: animal_property animal_property_animal_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_property
    ADD CONSTRAINT animal_property_animal_fk FOREIGN KEY (animal) REFERENCES animals.animal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: animal_property animal_property_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.animal_property
    ADD CONSTRAINT animal_property_fk FOREIGN KEY (property) REFERENCES animals.property(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cage2location cage2location_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage2location
    ADD CONSTRAINT cage2location_fk FOREIGN KEY (cage) REFERENCES animals.cage(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cage2location cage2location_fk_1; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage2location
    ADD CONSTRAINT cage2location_fk_1 FOREIGN KEY (location) REFERENCES animals.location(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cage2location cage2location_user_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cage2location
    ADD CONSTRAINT cage2location_user_fk FOREIGN KEY ("user") REFERENCES animals."user"(id);


--
-- Name: cost_account cost_account_user_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.cost_account
    ADD CONSTRAINT cost_account_user_fk FOREIGN KEY ("user") REFERENCES animals."user"(id);


--
-- Name: earmark earmark_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.earmark
    ADD CONSTRAINT earmark_fk FOREIGN KEY (animal) REFERENCES animals.animal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: experiment experiment_animal_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment
    ADD CONSTRAINT experiment_animal_fk FOREIGN KEY (animal) REFERENCES animals.animal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: experiment experiment_experiment_type_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment
    ADD CONSTRAINT experiment_experiment_type_fk FOREIGN KEY (experiment_type) REFERENCES animals.experiment_type(id);


--
-- Name: experiment experiment_permission_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.experiment
    ADD CONSTRAINT experiment_permission_fk FOREIGN KEY (permission) REFERENCES animals.permission(id);


--
-- Name: invoice invoice_cost_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.invoice
    ADD CONSTRAINT invoice_cost_fk FOREIGN KEY (cost_account) REFERENCES animals.cost_account(id);


--
-- Name: invoice invoice_property_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.invoice
    ADD CONSTRAINT invoice_property_fk FOREIGN KEY (property) REFERENCES animals.property(id);


--
-- Name: litter litter_import_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.litter
    ADD CONSTRAINT litter_import_fk FOREIGN KEY (import) REFERENCES animals.import(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: litter litter_mating_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.litter
    ADD CONSTRAINT litter_mating_fk FOREIGN KEY (mating) REFERENCES animals.mating(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mating_parent mating_parent_mating_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.mating_parent
    ADD CONSTRAINT mating_parent_mating_fk FOREIGN KEY (mating) REFERENCES animals.mating(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mating_parent mating_parent_parent_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.mating_parent
    ADD CONSTRAINT mating_parent_parent_fk FOREIGN KEY (parent) REFERENCES animals.animal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permission permission_user_fk; Type: FK CONSTRAINT; Schema: animals; Owner: -
--

ALTER TABLE ONLY animals.permission
    ADD CONSTRAINT permission_user_fk FOREIGN KEY ("user") REFERENCES animals."user"(id);


--
-- Name: SCHEMA animals; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA animals TO r_animals_ro;


--
-- Name: TABLE animal; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal TO r_animals_ro;
GRANT INSERT,UPDATE ON TABLE animals.animal TO r_animals_animal_rw;


--
-- Name: TABLE animal2cage; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal2cage TO r_animals_ro;
GRANT INSERT,UPDATE ON TABLE animals.animal2cage TO r_animals_animal_rw;


--
-- Name: SEQUENCE animal2cage_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.animal2cage_id_seq TO r_animals_animal_rw;


--
-- Name: TABLE animal2cage_last; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal2cage_last TO r_animals_ro;


--
-- Name: TABLE export; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.export TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.export TO r_animals_mating_rw;


--
-- Name: TABLE litter; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.litter TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.litter TO r_animals_mating_rw;


--
-- Name: TABLE animal_age; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_age TO r_animals_ro;


--
-- Name: TABLE animal_cost; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_cost TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.animal_cost TO r_animals_cost_rw;


--
-- Name: TABLE animal_cost_timespan; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_cost_timespan TO r_animals_ro;


--
-- Name: TABLE animal_property; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_property TO r_animals_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE animals.animal_property TO r_animals_property_rw;


--
-- Name: TABLE property; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.property TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.property TO r_animals_property_rw;


--
-- Name: TABLE animal_cost_accumulated_days; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_cost_accumulated_days TO r_animals_ro;


--
-- Name: SEQUENCE animal_cost_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.animal_cost_id_seq TO r_animals_cost_rw;


--
-- Name: TABLE animal_cost_last; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_cost_last TO r_animals_ro;


--
-- Name: SEQUENCE animal_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.animal_id_seq TO r_animals_animal_rw;


--
-- Name: TABLE animal_in_facility; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_in_facility TO r_animals_ro;


--
-- Name: TABLE animal_property_array; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_property_array TO r_animals_ro;


--
-- Name: TABLE animal_name; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.animal_name TO r_animals_ro;


--
-- Name: SEQUENCE animal_property_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.animal_property_id_seq TO r_animals_property_rw;


--
-- Name: TABLE cage; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.cage TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.cage TO r_animals_cage_rw;


--
-- Name: TABLE cage2location; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.cage2location TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.cage2location TO r_animals_cage_rw;


--
-- Name: SEQUENCE cage2location_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.cage2location_id_seq TO r_animals_cage_rw;


--
-- Name: TABLE cage2location_last; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.cage2location_last TO r_animals_ro;


--
-- Name: SEQUENCE cage_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.cage_id_seq TO r_animals_cage_rw;


--
-- Name: TABLE cost_account; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.cost_account TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.cost_account TO r_animals_cost_rw;


--
-- Name: TABLE cost_account_costs_accumulated; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.cost_account_costs_accumulated TO r_animals_ro;


--
-- Name: TABLE invoice; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.invoice TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.invoice TO r_animals_invoice_rw;


--
-- Name: TABLE cost_account_costs_next_invoice; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.cost_account_costs_next_invoice TO r_animals_ro;


--
-- Name: SEQUENCE cost_account_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.cost_account_id_seq TO r_animals_cost_rw;


--
-- Name: TABLE death_reason; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.death_reason TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.death_reason TO r_animals_legal_rw;


--
-- Name: SEQUENCE death_reason_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.death_reason_id_seq TO r_animals_legal_rw;


--
-- Name: TABLE earmark; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.earmark TO r_animals_ro;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE animals.earmark TO r_animals_property_rw;


--
-- Name: SEQUENCE earmark_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.earmark_id_seq TO r_animals_property_rw;


--
-- Name: TABLE experiment; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.experiment TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.experiment TO r_animals_experiment_rw;


--
-- Name: SEQUENCE experiment_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.experiment_id_seq TO r_animals_experiment_rw;


--
-- Name: TABLE experiment_type; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.experiment_type TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.experiment_type TO r_animals_experiment_rw;


--
-- Name: SEQUENCE experiment_type_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.experiment_type_id_seq TO r_animals_experiment_rw;


--
-- Name: SEQUENCE export_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.export_id_seq TO r_animals_mating_rw;


--
-- Name: TABLE import; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.import TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.import TO r_animals_mating_rw;


--
-- Name: SEQUENCE import_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.import_id_seq TO r_animals_mating_rw;


--
-- Name: SEQUENCE invoice_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.invoice_id_seq TO r_animals_invoice_rw;


--
-- Name: SEQUENCE litter_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.litter_id_seq TO r_animals_mating_rw;


--
-- Name: TABLE location; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.location TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.location TO r_animals_cage_rw;


--
-- Name: SEQUENCE location_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.location_id_seq TO r_animals_cage_rw;


--
-- Name: TABLE mating; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.mating TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.mating TO r_animals_mating_rw;


--
-- Name: TABLE mating_parent; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.mating_parent TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.mating_parent TO r_animals_mating_rw;


--
-- Name: TABLE mating_cost_account; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.mating_cost_account TO r_animals_ro;


--
-- Name: TABLE mating_death_permission; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.mating_death_permission TO r_animals_ro;


--
-- Name: SEQUENCE mating_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.mating_id_seq TO r_animals_mating_rw;


--
-- Name: TABLE mating_property; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.mating_property TO r_animals_ro;


--
-- Name: TABLE mating_inherited_overview; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.mating_inherited_overview TO r_animals_ro;


--
-- Name: TABLE mating_overview; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.mating_overview TO r_animals_ro;


--
-- Name: SEQUENCE mating_parent_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.mating_parent_id_seq TO r_animals_mating_rw;


--
-- Name: TABLE permission; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals.permission TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals.permission TO r_animals_legal_rw;


--
-- Name: SEQUENCE permission_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.permission_id_seq TO r_animals_legal_rw;


--
-- Name: SEQUENCE property_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.property_id_seq TO r_animals_property_rw;


--
-- Name: TABLE "user"; Type: ACL; Schema: animals; Owner: -
--

GRANT SELECT ON TABLE animals."user" TO r_animals_ro;
GRANT SELECT,INSERT,UPDATE ON TABLE animals."user" TO r_animals_user_rw;


--
-- Name: SEQUENCE user_id_seq; Type: ACL; Schema: animals; Owner: -
--

GRANT ALL ON SEQUENCE animals.user_id_seq TO r_animals_user_rw;


--
-- PostgreSQL database dump complete
--

