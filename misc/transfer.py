from iocbio.kinetics.app.fetch import fetch
import records

db = records.Database('postgresql://markov:PASSWORD@animal/animals')

# # breeding pairs
# for q in fetch('SELECT * FROM animals_mouse_breeding_pair'):
#     print(q)
#     db.query('INSERT INTO animals.mating(id, comment) VALUES (:id, :comment)',
#              id=q.id, comment=q.comments)

# # litters
# for q in fetch('SELECT * FROM animals_mouse_litter ORDER BY id'):
#     print(q)
#     db.query('INSERT INTO animals.litter(id, mating, birth, comment) VALUES (:id, :mating, :birth, :comment)',
#              id=q.id, mating=q.pair, birth=q.birth, comment=q.comments)

# # litters - fill in mating success
# for q in fetch('SELECT * FROM animals_mouse_litter ORDER BY id'):
#     print(q)
#     db.query('UPDATE animals.mating SET success=:success WHERE id=:id',
#              id=q.pair, success=True if q.success=='T' else False)

# # get spid map
# spid = {}
# for q in fetch("""
# select distinct am.species, s.spid
# from animals_mouse am 
# join prep_species s on am.species = s.id"""):
#     spid[q.species] = q.spid
# spid[None] = 'mouse C57BL6 generic'
# print(spid)
# for k in spid:
#     print("'%s': []," % spid[k])

# props = {}
# print('\n\n\n')
# for q in db.query('SELECT * FROM animals.property ORDER BY sorting_order,name'):
#     props[q.name] = q.id
#     print("'%s'" % q.name)

# print('\n\n\n')
# print(props)

# spid2props = {
#     'Mouse AGAT WT': ['Mouse', 'C57BL/6', 'AGAT', 'WT'],
#     'Mouse AGAT KO': ['Mouse', 'C57BL/6', 'AGAT', 'KO'],
#     'Mouse AGAT Heterozygous': ['Mouse', 'C57BL/6', 'AGAT', 'Het'],
#     'Mouse GAMT KO': ['Mouse', 'C57BL/6', 'GAMT', 'KO'],
#     'Mouse GAMT Heterozygous': ['Mouse', 'C57BL/6', 'GAMT', 'Het'],
#     'Mouse GAMT WT': ['Mouse', 'C57BL/6', 'GAMT', 'WT'],
#     'mouse GAMT Hets Oxford 2010.10': ['Mouse', 'C57BL/6', 'GAMT','Het' ],
#     'Sprague-Dawley rat': ['Rat', 'Sprague Dawley', 'WT'],
#     'mouse AGAT Hets Oxford 2010.10': ['Mouse', 'C57BL/6', 'AGAT','Het' ],
#     'Mouse AGAT KO + Cr': ['Mouse', 'C57BL/6', 'Cr fed','AGAT', 'KO' ],
#     'mouse C57BL6 generic': ['Mouse', 'C57BL/6', 'WT'],
# }    

# # animals
# for q in fetch('SELECT * FROM animals_mouse ORDER BY id'):
#     print(q)
#     db.query('INSERT INTO animals.animal(id, sex, litter, comment) VALUES (:id, :sex, :litter, :comment)',
#              id=q.id,
#              sex='F' if q.sex=='Female' else 'M',
#              litter=q.litter if q.litter is not None else 437,
#              comment=q.comments)
#     if q.death:
#         db.query('UPDATE animals.animal SET death=:death, death_reason=1, death_permission=1 WHERE id=:id',
#                  id=q.id, death=q.death)
#     for p in spid2props[spid[q.species]]:
#         print(q.species, spid[q.species], p, props[p])
#         db.query('INSERT INTO animals.animal_property(animal, property) VALUES (:animal, :pr)',
#                  animal=q.id, pr=props[p])
#     db.query('INSERT INTO animals.animal_cost(animal, cost_account) VALUES (:animal, :ca)',
#              animal=q.id, ca=1)
#     if q.earmark is not None:
#         db.query('INSERT INTO animals.earmark(animal, earmark) VALUES (:animal, :earmark)',
#                  animal=q.id, earmark=q.earmark)
        
# # breeding pairs parent
# for q in fetch('SELECT * FROM animals_mouse_breeding_pair_parent ORDER BY id'):
#     print(q)
#     if q.id>116 and q.parent is not None:
#         db.query('INSERT INTO animals.mating_parent(mating, parent) VALUES (:mating, :parent)',
#                  mating=q.pair, parent=q.parent)
