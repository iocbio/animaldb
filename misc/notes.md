# Notes regarding the database schema

## Limitations in consistency checks

As it may lead to database issues, there are no checks for some
consistency aspects in the tables. These checks have to be implemented
through interaction with the user.

### mating_parent

There is no check that the same parent is in the same mating more than
once when taking into account the time frame. In practice, the same
parent can be a part of several matings in parallel, can be moved
between cages several times during the same mating. The current design
allows this flexibility. However, it does allow putting the same
parent twice or more into the same mating with overlapping time. This
cannot be constrained via standard database constraints and has to be
checked on application level.

For checks, make a view showing mistakes and send email to operator
with found inconsistencies.

### animal_cost

As for `mating_parent`, `animal_cost` does not check for global
consistency. Checks that should be implemented via views and user
interaction:

* animals without associated costs accounts

* animals booked on more than one account at the same time. This
  includes booking the same animal twice on the same account.


## Saving the schema

Schema is saved using

```
pg_dump -n animals -O -s animals > misc/schema.sql
```

