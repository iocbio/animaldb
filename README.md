# IOCBIO WebDB

This is an application for animal facility monitoring. It is developed
for TalTech Animal Facility and is distributed as an open-source
project with the hope that it will be useful for other facilities.

Schema is stored in [`misc/schema.sql`](misc/schema.sql). See [WebDB
README](README.webdb.md) for details regarding setting it up.

