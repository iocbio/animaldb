# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
from pyramid.config import Configurator
from records import Database
from pyramid.session import SignedCookieSessionFactory, JSONSerializer
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import Allow, Everyone
from .site.routes import routes

import os, secrets, sys
import pyramid.security

class Root(object):
    __acl__ = [(Allow, 'group:users', 'application')]

    def __init__(self, request):
        pass

def groupfinder(userid, request):
    return ['group:users']

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """

    db_template = settings['database_template']
    db_user = settings['database_user']
    db_password = settings['database_password']

    db = Database(db_template.format(db_user, db_password))

    try:
        db.query('SELECT 1')
    except:
        print('Authentification failed for system user')
        sys.exit(-1)

    def get_db(request):
        return db
    
    config = Configurator(settings=settings, root_factory=Root)
    config.include('pyramid_jinja2')
    config.add_request_method(get_db, name='database', reify=True)

    session_factory = SignedCookieSessionFactory(secrets.token_urlsafe() + secrets.token_urlsafe(),
                                                 serializer=JSONSerializer())
    authn_policy = AuthTktAuthenticationPolicy(secrets.token_urlsafe() + secrets.token_urlsafe(),
                                               callback = groupfinder,
                                               hashalg = 'sha512')
    authz_policy = ACLAuthorizationPolicy()
    
    config.set_session_factory(session_factory)
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.set_default_permission('application')
    routes(config)

    config.scan()

    return config.make_wsgi_app()
