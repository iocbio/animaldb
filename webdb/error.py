# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#

# Exceptions used in the applications

class GenericError(Exception):
    def __init__(self, title, message, code = None):
        self.title = title
        self.message = message
        self.code = code

class SqlError(GenericError):
    def __init__(self, sqlerr):
        msg = \
            'Error while executing SQL statement. Full error is shown below and is ' + \
            'usually related to the absence of permissions or database connectivity.'
        GenericError.__init__(self,
                              title = 'Error: communication with the database',
                              message = msg,
                              code = sqlerr)
