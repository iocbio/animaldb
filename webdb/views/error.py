# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#

from pyramid.view import notfound_view_config, exception_view_config, view_config
from ..error import GenericError

@notfound_view_config(renderer='../templates/error.jinja2')
def notfound_view(exc, request):
    request.response.status = 404
    return dict( title = 'Page not found',
                 message = exc.message )

@view_config(route_name='error', renderer='../templates/error.jinja2')
def error(request):
    return dict( title='Error: %s' % request.params.get('title', 'No title specified'),
                 message = request.params.get('message', 'No message specified') )

@exception_view_config(GenericError, renderer='../templates/error.jinja2')
def generic_error(exc, request):
    request.response.status = 400
    return dict( title = exc.title,
                 message = exc.message,
                 code = exc.code )
    
