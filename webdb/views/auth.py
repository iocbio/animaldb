# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound
import pyramid.security

from ..db import check_user_login, check_web_use

@view_config(route_name='login', renderer='../templates/login.jinja2')
@forbidden_view_config(renderer='../templates/login.jinja2')
def login(request):    
    login_url = request.route_url('login')
    referrer = request.url
    if referrer.find(login_url) >= 0:
        referrer = '/'  # never use login form itself as came_from
    message = ''
    login = ''
    password = ''
    if 'login.form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        lcheck = check_user_login(request.registry.settings, login, password)
        if lcheck:
            testgroup = check_web_use(request.database, login)
            if testgroup:
                headers = pyramid.security.remember(request, login)
                return HTTPFound(location=request.params['next'],
                                 headers=headers)
            else: message = 'User is not configured to use web interface'
        else: message = 'Failed login'

    return dict(
        title='Login into %s' % request.registry.settings.get('main_title', 'IOCBIO WebDB'),
        message=message,
        url=request.route_url('login'),
        next_url=referrer,
        login=login,
        password=password
    )

@view_config(route_name='logout')
def logout(request):
    headers = pyramid.security.forget(request)
    return HTTPFound(location=request.route_url("home"), headers=headers)
    
