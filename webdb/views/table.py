# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from collections import deque
from ..db import query
from ..misc import *
from .text import Text
from ..error import GenericError

import sys, json, shlex

class TableView:
    """Helper class to view tables"""

    item_request_id = 'item_id'
    view_request_guide = 'view'
    edit_request_guide = 'edit'
    add_request_guide = 'add'
    remove_request_guide = 'remove'
    field_selector_field = 'field'
    field_selector_query = 'query'

    renderer_table = '../templates/table.jinja2'
    renderer_view = '../templates/table_item.jinja2'
    renderer_edit = '../templates/table_item_edit.jinja2'
    renderer_add =  '../templates/table_item_edit.jinja2' # same as edit

    # default options that can be overwritten
    can_add = True             # is it allowed to add new items
    can_remove = False         # is it allowed to remove items
    can_remove_simple = False  # is at allowed to remove items from table overview
    descendants = []           # list of columns and actions referring to items in this table

    def __init__(self, request):
        self.request = request

        # view and edit parameters
        self.item_id = request.params.get(self.item_request_id, '')
        self.field_selector_field = request.params.get(self.field_selector_field, '')
        self.field_selector_query = request.params.get(self.field_selector_query, '')
        self.flag_add_item  = True if self.add_request_guide in request.params else False
        self.flag_remove_item  = True if self.remove_request_guide in request.params else False
        self.flag_edit_item = True if self.edit_request_guide in request.params else False
        self.flag_view_item = True if self.view_request_guide in request.params else False

        # general vars
        self.back_to_list = self.request.params.get('back_to_list', '')
        do = self.request.params.get('data_original', '{}')
        do = do if do else '{}'
        self.data_original = json.loads(do)

        # search
        self.search = request.params.get('_search_', '')
        self.sql_filter, self.selector, self.searchStrings = self.parse_search()

        ids = set(f.id for f in self.fields)
        if self.field_selector_field and self.field_selector_field not in ids:
            self.request.session.flash('Column "%s" is not among possible selections for table "%s"' % (self.field_selector_field, self.db_table))
            self.field_selector_field = ''

        # order
        ids = [f.view for f in self.fields]
        self.order_reverse = True if 'order_reverse' in request.params else False
        self.order_by = request.params.get('order_by', ids[0])
        if self.order_by not in ids: self.order_by = ids[0]
        self.order = 'ORDER BY "' + self.order_by + '"'
        if self.order_reverse and self.order_by!=ids[0]: self.order += " DESC"
        elif not self.order_reverse and self.order_by==ids[0]: self.order += " DESC"

        # pagination
        self.items_per_page = str2int(request.registry.settings.get('table_items_per_page', ''), default=30)
        self.item_offset = str2int(request.params.get('item_offset', ''), default=0)

    def view(self):
        """Handle different possible calls (view table, add item, ...) to simplify
        declaration of derived classes and avoid specification of possible
        routing parameters in them"""

        from pyramid.renderers import render_to_response

        if self.can_add and self.flag_add_item:
            return render_to_response(self.renderer_add, self.edit_item(),
                                      request=self.request)
        elif self.item_id:
            if self.flag_edit_item:
                return render_to_response(self.renderer_edit, self.edit_item(),
                                          request=self.request)
            elif self.flag_view_item:
                return render_to_response(self.renderer_view, self.view_item(),
                                          request=self.request)
            elif self.flag_remove_item:
                # remove and let it render later to main view below
                self.remove_item()

        elif self.field_selector_field:
            return render_to_response('json', self.field_selector(),
                                      request=self.request)
        return render_to_response(self.renderer_table, self.view_table(),
                                  request=self.request)

    def view_table(self):
        """View table"""

        def url(offset=self.item_offset, selector=self.selector, search=self.search):
            q = dict(order_by=self.order_by)
            if search: q['_search_'] = search
            if 'desc' in self.request.params: q['desc'] = 1
            q['item_offset'] = offset
            return self.request.current_route_url(_query=q)

        # fill header
        header = []
        for f in self.fields:
            h = f.view
            qry = dict(order_by=h)
            if h == self.order_by and not self.order_reverse: qry['order_reverse'] = 1
            if self.search: qry['_search_'] = self.search
            txt = f.title
            if f.unit: txt += ', ' + f.unit
            header.append(Text(url=self.request.current_route_url(_query=qry),
                               text=txt, tooltip=f.view))

        data = deque(maxlen=self.items_per_page)
        data_index = 0
        some_left = False
        sql = 'SELECT * FROM (%s) s' % self.sql
        if self.sql_filter:
            sql += ' WHERE ' + self.sql_filter
        sql +=  ' ' + self.order
        for q in query(self.request, sql, **self.selector):
            d = []
            for f in self.fields:
                d.append(f.process(self,q))
                # first element should be ID and can be used for viewing it
                if len(d) == 1:
                    uq = dict(back_to_list=url())
                    uq[self.view_request_guide] = 1
                    uq[self.item_request_id] = q[f.id]
                    if self.search: uq['_search_'] = self.search
                    d[0].url = self.request.current_route_url(_query=uq)

            # add remove button if allowed
            if self.can_remove_simple:
                uq = dict(back_to_list=url())
                uq[self.item_request_id] = q[self.fields[0].id]
                uq[self.remove_request_guide] = 1
                d.append( Text(css_class='button button-remove-small', text='X',
                               url = self.request.current_route_url(_query=uq)) )

            # check if record complies with search string
            if len(self.searchStrings) < 1: found = True
            else:
                found = 0
                for ssind in range(len(self.searchStrings)):
                    ss = self.searchStrings[ssind]
                    for s in d:
                        if s.contains(ss, lower=True):
                            found += 1
                            break
                    if found < ssind + 1:
                        break # at least one string is missing already
                if found != len(self.searchStrings): found = False
                else: found = True

            # list if search string was found
            if found:
                data.append(d)
                if data.maxlen==len(data):
                    data_index += 1
                    if self.item_offset >= 0 and data_index > self.item_offset:
                        some_left = True
                        break

        # page controls
        if data_index > 0:
            pages = []
            if self.item_offset != 0:
                pages.append(Text(text="|<", url=url(0)))
            if data_index - self.items_per_page - 1 > 0:
                pages.append( Text(text="<", url=url(data_index - self.items_per_page - 1)) )
            if some_left:
                pages.append( Text(text=">", url=url(data_index + self.items_per_page - 1)) )
                pages.append(Text(text=">|", url=url(-1)))
        else:
            pages = False

        if self.can_add:
            uq = dict(back_to_list=url())
            uq[self.add_request_guide] = 1
            if self.search: uq['_search_'] = self.search
            add_url = self.request.current_route_url(_query=uq)
        else:
            add_url = False

        if self.sql_filter:
            restricted = self.sql_filter
            for k,v in self.selector.items():
                restricted = restricted.replace(':' + k, str(v))
            clear_url = url(selector=False, offset=0, search='')
        else:
            restricted, clear_url = "", ""
        if self.searchStrings:
            if restricted: restricted += ". "
            restricted += "Restricted by free search: " + ", ".join(self.searchStrings)

        return dict( title=self.title,
                     header=header,
                     data=data,
                     url=self.request.current_route_url(_query={}),
                     _search_ = self.search,
                     add_url = add_url,
                     restricted = restricted,
                     clear_url = clear_url,
                     pages = pages )

    def view_item(self):
        """View single item separately"""

        for q in query(self.request, self.sql + (' WHERE %s.%s=:id' % (self.sql_table,self.fields[0].id)), id=self.item_id):
            d = []
            for f in self.fields:
                t = f.process(self,q)
                d.append( dict(text = t,
                               field = f) )
            bl = self.back_to_list
            uq = dict(back_to_list = bl)
            uq[self.item_request_id] = self.item_id
            if self.search: uq['_search_'] = self.search
            descendants = []
            for des in self.descendants:
                if des.is_action:
                    descendants.append(Text(text = des.description,
                                            url = self.request.route_url(des.route,
                                                                         _query={ des.column: self.item_id })))
                else:
                    descendants.append(self.view_with_selector(text=des.description,
                                                               route=des.route,
                                                               column=des.column, value=self.item_id))

            result = dict( title=self.title + ': ID ' + self.item_id,
                           edit_url=self.request.current_route_url(_query={**uq, self.edit_request_guide: 1}),
                           data = d,
                           back_to_list = bl, descendants = descendants )

            if self.can_remove:
                result['remove_url'] = self.request.current_route_url(_query={**uq, self.remove_request_guide: 1})

            if self.can_add:
                result['add_url'] = self.request.current_route_url(_query={**uq, self.add_request_guide: 1})

            return result

        raise GenericError(title = 'Missing item',
                           message = 'Failed to find item in the database. Searching for item with ID = ' + \
                                     str(self.item_id),
                           code = self.sql)

    def edit_item(self):
        """Edit or add item"""

        import copy, dateutil.parser

        bl = self.back_to_list if self.back_to_list else self.request.current_route_url(_query={})

        def except_handle(extra_title):
            self.request.session.flash('Error: ' + str(sys.exc_info()[1]))
            title = '%s: %s' % (self.title, extra_title)
            raise HTTPFound(location=self.request.route_url(route_name="error",
                                                            _query=dict(title=title)))

        # Submitted form handling
        if 'item.form.submitted' in self.request.params:
            newval = {}
            for f in self.fields:
                if f.readonly: continue
                i = f.id
                if f.tags == tagDateTime and \
                   i + "_date_submit" in self.request.params and \
                   i + "_time" in self.request.params:
                    _d = self.request.params[i + "_date_submit"].strip()
                    _t = self.request.params[i + "_time"].strip()
                    if _d or _t:
                        try:
                            newval[i] = dateutil.parser.isoparse( _d + " " + _t).isoformat()
                        except:
                            except_handle('Time and date string entry for ' + i + ' column')
                    else:
                        newval[i] = ''
                elif f.tags == tagDate and \
                     i + "_date_submit" in self.request.params:
                    _d = self.request.params[i + "_date_submit"].strip()
                    if _d:
                        try:
                            newval[i] = dateutil.parser.isoparse( _d ).date().isoformat()
                        except:
                            except_handle('Date string entry for ' + i + ' column')
                    else:
                        newval[i] = ''
                elif f.tags == tagTrueFalse and i not in self.request.params:
                    newval[i] = 'F' # was not checked
                elif f.tags == tagBool and i not in self.request.params:
                    newval[i] = False # was not checked
                elif f.tags == tagBool and self.request.params[i].strip() == 'T':
                    newval[i] = True # was checked
                elif i in self.request.params:
                    newval[i] = self.request.params[i].strip()

            orig = self.data_original
            changed_ids = []
            for f in self.fields[1:]:
                if f.readonly: continue
                i = f.id
                if i not in orig and i in newval:
                    changed_ids.append(i)
                elif str(orig.get(i, '')) != str(newval.get(i, '')):
                    changed_ids.append(i)

            if self.flag_edit_item:
                if changed_ids:
                    # set to NULL when type requires it
                    for f in self.fields[1:]:
                        if f.id not in changed_ids: continue
                        i = f.id
                        if i in newval and newval[i] == '' and \
                           (f.tags==tagFloat or f.tags==tagInt or f.tags==tagDateTime or
                            f.tags==tagDate):
                            newval[i] = None

                    # generate update statement
                    s = ('UPDATE %s.%s' % (self.db_schema, self.db_table)) + ' SET '
                    for i in changed_ids:
                        s += '"%s"=:%s, '% (i,i)
                    s = s[:-2] + ' WHERE "%s"=:%s' % (self.fields[0].id, self.fields[0].id)
                    try:
                        query(self.request, s, **newval)
                        self.request.session.flash(self.title + ' item %s saved' % self.item_id)
                    except:
                        except_handle('Edit item ' + self.item_id)
                else:
                    self.request.session.flash('There are no changes after editing, nothing to save')

                item_id = self.request.params[ self.fields[0].id ]

            elif self.flag_add_item:
                if changed_ids:
                    desc, vals = '(', 'VALUES ('
                    for i in changed_ids:
                        desc += '"' + i + '",'
                        vals += ':' + i + ','
                    desc = desc[:-1] + ')'
                    vals = vals[:-1] + ')'
                else:
                    vals = 'DEFAULT VALUES'
                    desc = ''

                try:
                    for q in query(self.request, 'INSERT INTO %s.%s %s %s RETURNING %s' %
                                   (self.db_schema, self.db_table, desc, vals, self.fields[0].id),
                                   **newval):
                        item_id = q[0]
                    self.request.session.flash(self.title + ' item %s saved' % self.item_id)
                except:
                    except_handle('Add item')

            uq = dict(back_to_list = bl)
            uq[self.view_request_guide] = 1
            uq[self.item_request_id] = item_id
            uq['select'] = self.encode_selector(self.selector)
            uq['_search_'] = self.search
            raise HTTPFound(location=self.request.current_route_url(_query=uq))

        # The first call - without submitted form

        # prepare selectors
        selections = {}
        for f in self.fields:
            if f.selector and not f.large_selection:
                s = f.selector
                if isinstance(s, str): selections[f.id] = [dict(value=q.value, text=q.text) for q in query(self.request, s)]
                else: selections[f.id] = copy.copy(s)
                selections[f.id].insert(0, dict(value='', text=''))

        data_original = {}
        d = []
        if self.flag_edit_item:
            # main query - only one item is expected
            for q in query(self.request, self.sql + (' WHERE %s.%s=:id' % (self.sql_table,self.fields[0].id)), id=self.item_id):
                d = []
                for f in self.fields:
                    if f.readonly: continue
                    i = f.id
                    data_original[i] = str(q[i]).strip() if q[i] is not None else ''
                    t = Text(text=q[i]) if q[i] is not None else Text()
                    tags = f.tags

                    dateTime = True if tags == tagDateTime else False
                    dateOnly = True if tags == tagDate else False
                    trueFalse = True if tags == tagTrueFalse or tags == tagBool else False
                    if (tags == tagTrueFalse and t.text=='T') or (tags == tagBool and t.text):
                        checked = True
                    else: checked = False
                    readonly = True if f.id == self.fields[0].id or f.readonly or f.id in self.selector else False
                    item = dict(id = i,
                                text = t,
                                readonly = readonly,
                                selection = selections.get(i, False),
                                largeSelection = f.large_selection,
                                largeSelectionText = self.field_selector_text(field=i, value=t.text) if f.large_selection else '',
                                field = f,
                                textArea = True if tags == tagText else False,
                                dateTime = dateTime, dateOnly = dateOnly,
                                trueFalse = trueFalse, checked = checked )

                    if dateTime:
                        item['date'] = q[i].date().isoformat() if q[i] is not None else ''
                        item['time'] = q[i].time().isoformat(timespec='minutes') if q[i] is not None else ''
                        data_original[i] = q[i].isoformat() if q[i] is not None else ''
                    if dateOnly:
                        item['date'] = q[i].isoformat() if q[i] is not None else ''
                        data_original[i] = q[i].isoformat() if q[i] is not None else ''

                    d.append(item)

        elif self.flag_add_item:
            # get database defaults and fill accordingly
            d = []
            dtmp = {}
            ids = {f.id: f for f in self.fields}
            for q in query(self.request,
                           """
SELECT column_name, column_default
FROM information_schema.columns
WHERE (table_schema, table_name) = (:schema, :table) AND column_name <> :id
ORDER BY ordinal_position""", schema = self.db_schema, table = self.db_table, id = self.fields[0].id):
                i = q.column_name
                if i not in ids:
                    self.request.session.flash('Error: column ' + i + ' not among given IDs')
                    title = self.title + ': Add new item'
                    raise HTTPFound(location=self.request.route_url(route_name="error",
                                                                    _query=dict(title=title)))
                if q.column_default is None: default = ''
                else: default = str(q.column_default)
                data_original[i] = ''
                f = ids[i]
                t = Text(text=self.selector.get(f.id, ''))
                tags = f.tags

                # check if it has application-specific default
                if f.add_default is not None:
                    if callable(f.add_default): t.text = f.add_default(self.request)
                    else: t.text = f.add_default

                dateTime = True if tags == tagDateTime else False
                dateOnly = True if tags == tagDate else False
                trueFalse = True if tags == tagTrueFalse or tags == tagBool else False
                if (tags == tagTrueFalse and t.text=='T') or (tags == tagBool and t.text):
                    checked = True
                else: checked = False
                readonly = True if f.id == self.fields[0].id or f.readonly or f.id in self.selector else False
                item = dict(id = i,
                            text = t,
                            readonly = readonly,
                            selection = selections.get(i, False),
                            largeSelection = f.large_selection,
                            field = f,
                            textArea = True if tags == tagText else False,
                            dateTime = dateTime, dateOnly = dateOnly,
                            trueFalse = trueFalse, checked = checked,
                            default = default )

                if dateTime:
                    item['date'] = t.text.date().isoformat() if t.text else ''
                    item['time'] = t.text.time().isoformat(timespec='minutes') if t.text else ''
                    data_original[i] = ''
                if dateOnly:
                    item['date'] = t.text.isoformat() if t.text else ''
                    data_original[i] = ''

                dtmp[i] = item
            d = [ dtmp[f.id] for f in self.fields if f.id in dtmp ]

        if not d: raise GenericError(title='Empty item',
                                     message='There is nothing to include in the form for adding or editing an item')

        uq = dict()
        title = ''
        if self.flag_edit_item:
            uq[self.edit_request_guide] = 1
            uq[self.item_request_id] = self.item_id
            title = '%s: Edit item %s' % (self.title, self.item_id)
        elif self.flag_add_item:
            uq[self.add_request_guide] = 1
            title = '%s: Add new item' % (self.title)

        return dict( title = title,
                     edit_url = self.request.current_route_url(_query=uq),
                     data = d,
                     data_original = json.dumps(data_original),
                     back_to_list = bl,
                     _search_ = self.search )

    def remove_item(self):
        newval = {}
        newval[ self.fields[0].id ] = self.item_id
        items_removed = ''
        for q in query(self.request,
                       'DELETE FROM %s.%s WHERE "%s"=:%s RETURNING *' % (self.db_schema, self.db_table,
                                                                         self.fields[0].id, self.fields[0].id),
                       **newval):
            for k in q.keys():
                if q[k] is not None:
                    items_removed += k + ": " + str(q[k]) + ' / '
            items_removed += "\n"
        if len(items_removed):
            msg = 'Removed item:\n ' + items_removed
        else:
            msg = 'Failed to remove item with ID=' + self.item_id
        self.request.session.flash(msg)
        bl = self.back_to_list if self.back_to_list else self.request.current_route_url(_query={})
        raise HTTPFound(location=bl)

    def field_selector(self):
        """Return selection based on the query for a field"""

        response = []
        for f in self.fields:
            if f.id == self.field_selector_field and f.selector:
                for q in query(self.request,
                               "SELECT * FROM (%s) s WHERE lower(text) LIKE lower(:q) LIMIT 10" % f.selector,
                               q = '%' + self.field_selector_query + '%'):
                    response.append(dict(value = q.value, text = q.text))
                return response
        return response

    def field_selector_text(self, field, value):
        """Return text corresponding to the selected value"""

        for f in self.fields:
            if f.id == field and f.selector:
                for q in query(self.request, "SELECT * FROM (%s) s WHERE value=:value" % f.selector,
                               value=value):
                    return q.text
        return ''


    # helper methods

    def view_link(self, text, route, item_id, css_class=''):
        """Create Text with url link for viewing an item in a table behind route"""
        return Text(text = text,
                    url = self.request.route_url(route,
                                                 _query={ TableView.item_request_id: item_id,
                                                          TableView.view_request_guide: 1 }),
                    css_class = css_class )

    def view_with_selector(self, text, route, selector={}, column=None, value=None):
        """Create Text with url link for viewing an item in a table behind route and given selector"""
        if not selector:
            selector = { column: value }
        search = []
        for k, v in selector.items():
            if v is None: search.append('%s=NULL' % k)
            else: search.append('%s="%s"' % (k,v))
        search = ' '.join(search)
        return Text(text = text,
                    url = self.request.route_url(route, _query = dict(_search_=search)))

    def session_store(self, prefix, value):
        """Store value in session in randomly generated key"""
        import random, string
        repkey = prefix + '-' + ''.join(random.choices(string.ascii_letters + string.digits, k=8))
        self.request.session[repkey] = value
        return repkey

    def session_load(self, key, errormsg=None, errorurl=None, removekey=True):
        """Load data from session using key and optionally remove it from session storage"""
        if key in self.request.session:
            data = self.request.session[key]
            if removekey:
                del self.request.session[key]
        else:
            if errormsg:
                self.request.session.flash(errormsg)
            if errorurl:
                if errorurl == True:
                    errorurl = self.request.current_route_url(_query={})
                raise HTTPFound(location = errorurl)
            return None
        return data

    # parsing search string
    def parse_search(self):
        """Parse and generate SQL query parts that correspond to tokens

        Query parts requiring that some field is equal to something
        are passed to selector and can be used to restrict data entry
        accordingly. This does not extend to the fields consisting of
        arrays where equal is interpreted as having such a member as an
        element.
        """

        def nextkey(selector, key):
            cnt = sum(1 for _ in selector.keys())
            return "%s_%d" % (key, cnt)

        tokens = shlex.split(self.search)
        text_strings = []
        selector = {}
        sql = []
        for t in tokens:
            claimed = False
            operators = ['>=', '<=', '!=', '<>', '>', '<', '=']
            for oper in operators:
                spl = t.split(oper)
                if len(spl) == 2:
                    f = None
                    key, value = spl[0], spl[1]
                    for i in self.fields:
                        if i.id==key or i.view==key:
                            f = i
                            break
                    if f is None:
                        code = 'Search string: ' + self.search + '\n\nAvailable fields:\n'
                        fld = set()
                        for i in self.fields:
                            fld.add(i.id)
                            fld.add(i.view)
                        fld = list(fld)
                        fld.sort()
                        code += '\n'.join(fld)
                        raise GenericError(title = 'Error in search string',
                                           message = 'Cannot find field specified in the search string: %s' % key,
                                           code = code)

                    if f.array:
                        nk = nextkey(selector, key)
                        if oper == '=':
                            sql.append('ARRAY_POSITION(%s, :%s) IS NOT NULL' % (key, nk))
                            selector[nk] = value
                        elif oper in ['<>', '!=']:
                            sql.append('ARRAY_POSITION(%s, :%s) IS NULL' % (key, nk))
                            selector[nk] = value
                        else:
                            raise GenericError(title = 'Error in search string',
                                               message = \
                                               'Cannot use operator "%s" on the field "%s" with arrays' % (key, oper))
                        claimed = True
                    else:
                        nk = nextkey(selector, key)
                        if oper == '=' and value == 'NULL':
                            sql.append('%s IS NULL' % key)
                            selector[nk] = None
                        elif oper in ['<>', '!='] and value == 'NULL':
                            sql.append('%s IS NOT NULL' % key)
                            selector[nk] = None
                        elif oper == '=':
                            sql.append('%s=:%s' % (key, key))
                            selector[key] = value
                        else:
                            sql.append('%s%s:%s' % (key, oper, nk))
                            selector[nk] = value
                        claimed = True
                if claimed: break

            if not claimed:
                text_strings.append(t)
        return ' AND '.join(sql), selector, text_strings


    @staticmethod
    def encode_selector(selector):
        return json.dumps(selector)
