## This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
# Helper classes for data access

class Text:
    """Text fields for rendering"""

    def __init__(self, text='', url='', css_class='', tooltip=''):
        if isinstance(text, list):
            self.children = [Text(t, url, css_class, tooltip) for t in text]
            self.text = ''
            self.url = ''
            self.css_class = ''
            self.tooltip = ''
        else:
            self.children = None
            self.text = text if text is not None else ''
            if self.text:
                self.url = url[text] if isinstance(url, dict) else url
                self.css_class = css_class[text] if isinstance(css_class, dict) else css_class
                self.tooltip = tooltip if isinstance(tooltip, dict) else tooltip
            else:
                self.url = ''
                self.css_class = ''
                self.tooltip = ''

    def __str__(self):
        return 'url=' + str(self.url) + ' / text=' + str(self.text)

    def contains(self, search, lower=False):
        if self.children:
            for c in self.children:
                if c.contains(search, lower):
                    return True
            return False
        return (self.find(search, lower) >= 0)

    def find(self, search, lower=False):
        if lower:
            s = str(self.text).lower()
            return s.find(search.lower())
        return self.text.find(search)


class Field:
    """Representation of the database record field"""

    def __init__(self,
                 id,
                 title = None, comment='', unit='',
                 process=None, selector=None, large_selection=False,
                 readonly=False, view=None, tags={}, add_default=None, array = False):
        self.id = id
        self.title = title if title is not None else id.replace('_', ' ').title()
        self.comment = comment                  # comment for the field
        self.unit = unit                        # unit
        self._process = process                 # process the record using functions working on self,row. Used in table and item views
        self.selector = selector                # SELECT statements returning value,text for column
        self.large_selection = large_selection if selector else False   # Set to True when selector returns large list
        self.readonly = readonly                # whether field is readonly
        self.view = view if view is not None else id                    # show this value instead in view item or in table
        self.tags = tags                        # HTML or data type tags
        self.add_default = add_default          # used while adding new element; can be callable or a value
        self.array = array                      # used for SQL results leading to array elements

    def process(self, table, q):
        from ..misc import tagTrueFalse, tagBool, isTrue
        if self._process:
            return self._process(table, q)
        txt = q[self.view] if q[self.view] is not None else ''
        if self.tags == tagTrueFalse or self.tags == tagBool:
            if txt == isTrue or (self.tags == tagBool and txt): txt = '✓'
            else: txt = ''
        return Text(text=txt)


class Descendant:
    """Description of an item in a table referring to parent"""

    def __init__(self, route, column, description, is_action=False):
        self.route = route
        self.column = column
        self.description = description
        self.is_action = is_action
