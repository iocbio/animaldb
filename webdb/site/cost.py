# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from ..views.table import TableView
from ..views.text import *
from ..misc import *

from .animal import Property
from .user import User
from .selector import Sel

@view_defaults(route_name='cost_account')
class CostAccount(TableView):
    sql = """
    SELECT c.id, c.name, c.active,
    c.user, u.name AS user_name,
    curr.animals,
    c.comment
    FROM animals.cost_account c
    LEFT JOIN animals.user u ON c.user = u.id
    LEFT JOIN (
    SELECT COUNT(*) as animals, cost_account
    FROM animals.animal_in_facility am
    JOIN animals.animal_cost_last acl ON acl.animal=am.id
    GROUP BY cost_account) curr ON curr.cost_account=c.id
    """
    sql_table = "c"

    db_schema = 'animals'
    db_table  = 'cost_account'
    title = 'Cost Accounts'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name', title='Name' ),
        Field( 'active', comment='Is account active', tags=tagBool ),
        Field( 'user', view='user_name', title = 'Contact',
               selector=User.refSelection,
               process=lambda self, r: self.view_link(text = r.user_name, route = 'user', item_id=r.user) ),
        Field( 'animals', title='# Active Animals', readonly=True,
               comment='Number of animals in facility associated with this cost account'),
        Field( 'comment', tags=tagText )
    ]

    refSelection = "SELECT c.id AS value, c.name AS text FROM animals.cost_account c ORDER BY c.active DESC, c.name"

    descendants = [
        Descendant( 'animal_cost', 'cost_account', 'Animal costs' ),
        Descendant( 'invoice', 'cost_account', 'Invoices' ),
        Descendant( 'cost_account_costs_next_invoice', 'id', 'Costs since last invoice' ),
        Descendant( 'cost_account_costs_accumulated', 'id', 'Accumulated costs' ),
        Descendant( 'mating', 'cost_account', 'Matings' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='animal_cost')
class AnimalCost(TableView):
    sql = """
    SELECT ac.id,
    ac.animal, am.name AS animal_name,
    ac.cost_account, ca.name AS cost_account_name,
    ac.from, ac.till, ac.comment
    FROM animals.animal_cost ac
    JOIN animals.animal_name am ON am.id=ac.animal
    JOIN animals.cost_account ca ON ca.id=ac.cost_account
    """
    sql_table = "ac"

    db_schema = 'animals'
    db_table  = 'animal_cost'
    title = 'Animal costs'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'animal', view = 'animal_name',
               large_selection = True, selector = Sel.animalSelect,
               process = lambda self, r: self.view_link(text = r.animal_name,
                                                        route = 'animal', item_id=r.animal) ),
        Field( 'cost_account', view='cost_account_name',
               selector=CostAccount.refSelection,
               process=lambda self, r: self.view_link(text = r.cost_account_name, route = 'cost_account',
                                                      item_id=r.cost_account) ),
        Field( 'from', tags=tagDate ),
        Field( 'till', tags=tagDate ),        
        Field( 'comment', tags=tagText )
    ]
    
    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='cost_account_costs_accumulated')
class CostAccumulated(TableView):
    sql = """
    SELECT ac.id, ca.name AS cost_account_name,
    ac.property, p.name AS property_name,
    ac.amount
    FROM animals.cost_account_costs_accumulated ac
    JOIN animals.cost_account ca ON ca.id=ac.id
    JOIN animals.property p ON p.id=ac.property
    """
    sql_table = "ac"

    db_schema = 'animals'
    db_table  = 'animal_cost'
    title = 'Accumulated costs'

    can_add = False

    fields = [
        Field( 'id', title='ID', comment='Cost account ID' ),
        Field( 'cost_account_name', title='Cost Account',
               process=lambda self, r: self.view_link(text = r.cost_account_name, route = 'cost_account',
                                                      item_id=r.id) ),
        Field( 'property', view='property_name',
               process = lambda self, r: self.view_link(text = r.property_name, 
                                                        route = 'property', item_id=r['property']) ),
        Field( 'amount', title='Accumulated days' )
    ]
    
    @view_config()
    def view(self): return TableView.view(self)

    
@view_defaults(route_name='cost_account_costs_next_invoice')
class CostSinceLastInvoice(TableView):
    sql = """
    SELECT ac.id, ca.name AS cost_account_name,
    ac.property, p.name AS property_name,
    ac.amount
    FROM animals.cost_account_costs_next_invoice ac
    JOIN animals.cost_account ca ON ca.id=ac.id
    JOIN animals.property p ON p.id=ac.property
    """
    sql_table = "ac"

    db_schema = 'animals'
    db_table  = 'animal_cost'
    title = 'Costs since last invoice'

    can_add = False

    fields = [
        Field( 'id', title='ID', comment='Cost account ID' ),
        Field( 'cost_account_name', title='Cost Account',
               process=lambda self, r: self.view_link(text = r.cost_account_name, route = 'cost_account',
                                                      item_id=r.id) ),
        Field( 'property', view='property_name',
               process = lambda self, r: self.view_link(text = r.property_name, 
                                                        route = 'property', item_id=r['property']) ),
        Field( 'amount', title='Accumulated days' )
    ]
    
    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='invoice')
class Invoice(TableView):
    sql = """
    SELECT inv.id, inv.when,
    inv.cost_account, ca.name AS cost_account_name,
    inv.property, p.name AS property_name,
    inv.amount, inv.comment
    FROM animals.invoice inv
    JOIN animals.cost_account ca ON ca.id=inv.cost_account
    JOIN animals.property p ON p.id=inv.property
    """
    sql_table = "inv"

    db_schema = 'animals'
    db_table  = 'invoice'
    title = 'Invoices'

    can_add = True
    can_edit = False

    fields = [
        Field( 'id', title='ID' ),
        Field( 'when', tags=tagDateTime ),
        Field( 'cost_account', view='cost_account_name', title='Cost Account',
               selector=CostAccount.refSelection,
               process=lambda self, r: self.view_link(text = r.cost_account_name, route = 'cost_account',
                                                      item_id=r.cost_account) ),
        Field( 'property', view='property_name',
               selector=Property.refSelection,
               process = lambda self, r: self.view_link(text = r.property_name, 
                                                        route = 'property', item_id=r['property']) ),
        Field( 'amount', title='Accumulated days' ),
        Field( 'comment', tags=tagText )
    ]
    
    @view_config()
    def view(self): return TableView.view(self)

