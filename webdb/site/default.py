# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#
from pyramid.view import view_config
from .routes import Routing

@view_config(route_name='home', renderer='../templates/index.jinja2')
def index(request):
    R = {}
    for rk in Routing:
        r = []
        for i in Routing[rk]:
            if i.visible: r.append(i)
        R[rk] = r
        
    return dict(title = request.registry.settings.get('main_title', 'IOCBIO Animals'),
                routes = R)

