# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound
from attrdict import AttrDict

from ..views.form import FormView
from ..views.text import *
from ..misc import *
from ..db import query
from ..error import GenericError

from .selector import Sel
from .cage_location import Cage
from .user import Permission, User
from .cost import CostAccount

@view_defaults(route_name='action_add_animals')
class ActionAddAnimal(FormView):

    fields = [
        # order is used below in confirm view
        Field( 'litter',
               selector=Sel.litterSelect, tags=tagInt,
               large_selection = True,
               process=lambda self, r: self.view_link(text = r.litter, route = 'litter', item_id=r.litter) ),
        Field( 'female', tags=tagInt ),
        Field( 'male', tags=tagInt ),
        Field( 'comment', tags=tagText ),
        Field( 'user', title='User', view='user_name', selector=User.refSelection,
               comment='User who moved the animal into the cage',
               add_default = User.addDefault ),
    ]
        
    def __init__(self, request):
        FormView.__init__(self, request)
        self.litter = self.request.params.get('litter', '')
        self.female = self.request.params.get('female', '')
        self.male = self.request.params.get('male', '')
        self.comment = self.request.params.get('comment', '')
        self.cage = self.request.params.get('cage', None)
        self.user = self.request.params.get('user', None)

    @view_config(renderer='../templates/table_item_edit.jinja2')
    def litter_entry(self):
        import copy
        
        # no litter selected, force selection
        if not self.litter:
            return self.fill_form(fields = self.fields[0:1],
                                  title = 'Add animals to litter: Select litter')

        inh_properties = []
        d_permission, cost_account = '', ''
        for q in query(self.request,
                       """
                       SELECT m.mating, m.properties, m.death_permission, m.cost_account
                       FROM animals.litter l
                       JOIN animals.mating_inherited_overview m ON m.mating=l.mating
                       WHERE l.id=:litter
                       """, litter=self.litter):
            if q.properties is not None:
                inh_properties = q.properties
            d_permission = q.death_permission
            cost_account = q.cost_account

        fields = copy.copy(self.fields)

        # permission and cost account
        fields.append(Field( 'death_permission', add_default = d_permission,
                             selector = Permission.refSelection ))
        fields.append(Field( 'cost_account', title='Cost account', add_default = cost_account,
                             selector=CostAccount.refSelection ))
        
        # cage
        fields.append(Field( 'cage', title='Cage', selector=Cage.refSelection ))
        
        # properties
        for q in query(self.request,
                       """
                       SELECT p.id AS property, p.name AS property_name
                       FROM animals.property p
                       ORDER BY p.sorting_order, p.name
                       """):
            fields.append(Field('property_' + q.property_name, title='Property: ' +  q.property_name,
                                tags=tagBool, add_default=(q.property in inh_properties)))
                       
        return self.fill_form(fields = fields,
                              title = 'Add animals to litter',
                              edit_url = self.request.current_route_url(_query={'confirm_form_submission': 1}))

    def process_input(self):
        self.female = int(self.female) if self.female else 0
        self.male = int(self.male) if self.male else 0
        if not self.litter:
            raise GenericError(title='Error while adding animals',
                               message='Error: No litter specified')
        if self.male+self.female == 0:
            raise GenericError(title='Error while adding animals',
                               message='Error: No animals to add')
        if self.male<0 or self.female<0:
            raise GenericError(title='Error while adding animals',
                               message='Error: Number of animals cannot be negative')
        if self.male+self.female > 40:
            raise GenericError(title='Error while adding animals',
                               message='Number of animals to add larger than limit of 40. ' +
                                       'Please split the animals into subgroups.')
        if bool(self.cage) != bool(self.user):
            raise GenericError(title='Error while adding animals',
                               message='Both cage and user have to be specified. Either specify the both or none.')
            
            
    @view_config(renderer='../templates/table_item.jinja2', request_param='confirm_form_submission')
    def confirm(self):
        self.process_input()
        d = []
        d.append(dict(text = Text(self.litter), field=self.fields[0])) # litter
        d.append(dict(text = Text(self.female), field=self.fields[1])) # female
        d.append(dict(text = Text(self.male), field=self.fields[2])) # male
        d.append(dict(text = Text(self.comment), field=self.fields[3])) # comment
        
        for k in [ AttrDict( k='death_permission', sql=Permission.refSelection ),
                   AttrDict( k='cost_account', sql=CostAccount.refSelection ),
                   AttrDict( k='user', sql=User.refSelection ),
                   AttrDict( k='cage', sql=Cage.refSelection ) ]:
            if self.request.params.get(k.k, ''):
                d.append(dict(text=Text(self.value2text(k.sql, key=k.k)),
                                        field=Field(k.k)))

        for k in self.request.params.keys():
            if k.find('property_') == 0 and self.request.params[k]=='T':
                d.append(dict(text=Text('✓'), field=Field(k,
                                                          title='Property: ' + k[len('property_'):])))

        uq = {k: v for k,v in self.request.params.items()}
        uq['perform_addition']=self.session_store('add', 1)
        del uq['confirm_form_submission']
        return dict( title='Confirm addition of animals',
                     edit_url=self.request.current_route_url(_query=uq),
                     edit_button_text='Add animals',
                     data = d,
                     back_to_list = self.back_to_list )


    @view_config(renderer='../templates/animaldb_list.jinja2', request_param='perform_addition')
    def action(self):
        self.process_input()

        # check if addition has been used already
        self.session_load(key = self.request.params.get('perform_addition', '<>'),
                          errormsg = 'Animal addition expired',
                          errorurl = True )

        death_permission = self.request.params.get('death_permission', None)
        cost_account = self.request.params.get('cost_account', None)

        if death_permission=='': death_permission = None

        animids = []
        for I in [ AttrDict(sex='M', nr=self.male),
                   AttrDict(sex='F', nr=self.female) ]:
            for i in range(I.nr):
                animal = query(self.request,
                               """
                               INSERT INTO animals.animal (sex, litter, death_permission, comment)
                               VALUES (:sex, :litter, :death_permission, :comment) RETURNING id
                               """, sex=I.sex, litter=self.litter,
                               death_permission=death_permission, comment=self.comment).first()[0]
                animids.append(animal)
                # add cost account if available
                if cost_account:
                    query(self.request,
                          "INSERT INTO animals.animal_cost (animal, cost_account) VALUES (:animal, :ca)",
                          animal=animal, ca=cost_account)
                # cage
                if self.cage:
                    query(self.request,
                          'INSERT INTO animals.animal2cage (animal, cage, "user") VALUES (:animal, :cage, :user)',
                          animal=animal, cage=self.cage, user=self.user)
                # properties
                for k in self.request.params.keys():
                    if k.find('property_')==0:
                        pname = k[len('property_'):]
                        pid = query(self.request,
                                    "SELECT id FROM animals.property WHERE name=:name",
                                    name=pname).first()[0]
                        query(self.request,
                              "INSERT INTO animals.animal_property (animal, property) VALUES (:animal, :property)",
                              animal=animal, property=pid)
                             
        url = self.request.current_route_url(_query=dict(report=self.session_store('report', animids),
                                                         litter = self.litter))
        raise HTTPFound(location = url)

    @view_config(renderer='../templates/animaldb_list.jinja2', request_param='report')
    def report(self):
        animals = self.session_load(key = self.request.params.get('report', '<>'),
                                    errormsg = 'Animal addition report expired',
                                    errorurl = True )
    
        header = [Text('Animal')]
        d = []
        for aid in animals:
            q = query(self.request,
                      "SELECT n.id, n.name FROM animals.animal_name n WHERE n.id=:aid",
                      aid=aid).first()
            d.append([self.view_link(text = q.name,
                                     route = 'animal', item_id=aid)])

        res = self.view_with_selector('<>', 'animal', column='litter', value=self.litter)
        return dict( title='Done! Added %d animals to litter %s' % (len(animals), self.litter ),
                     goto_url=res.url,
                     goto_button_text='See all animals in litter ' + self.litter,
                     data = d,
                     header = header )
                

    @view_config(renderer='json', request_param=['field', 'query'])
    def field_selector(self):
        """Support for large selectors"""
        return FormView.field_selector(self)
