# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from attrdict import AttrDict

from ..views.form import FormView
from ..views.text import *
from ..misc import *
from ..db import query

from .cost import CostAccount

@view_defaults(route_name='action_new_invoices')
class ActionNewInvoices(FormView):

    fields = [ Field('ID') ]

    def __init__(self, request):
        FormView.__init__(self, request)

    @view_config(renderer='../templates/animaldb_question.jinja2')
    def starter(self):
        q = dict(create_invoice=self.session_store('create_invoice', 1))
        return dict(title="Create invoices",
                    content="Create invoices based on the current balance?",
                    action_button_text="Create",
                    action_url = self.request.current_route_url(_query=q))

    @view_config(request_param='create_invoice')
    def action(self):
        # check for expiry
        self.session_load(key = self.request.params.get('create_invoice', '<>'),
                          errormsg = 'Invoice creation expired, try again',
                          errorurl = True )
        invoices = []
        for q in query(self.request,
                       """
                       INSERT INTO animals.invoice (cost_account, "property", amount)
                       SELECT id, "property", amount FROM animals.cost_account_costs_next_invoice
                       RETURNING id
                       """):
            invoices.append(q.id)
        url = self.request.current_route_url(_query=dict(created_invoices=self.session_store('invoices', invoices)))
        raise HTTPFound(location = url)

    @view_config(renderer='../templates/animaldb_list.jinja2', request_param='created_invoices')
    def report(self):
        invoices = self.session_load(key = self.request.params.get('created_invoices', '<>'),
                                     errormsg = 'Created invoices report expired, showing all',
                                     errorurl = self.request.route_url('invoice') )
        
        header = [Text('Invoices'), Text('Cost Acoount'), Text('Property'), Text('Amount')]
        d = []
        for i in invoices:
            q = query(self.request,
                      """
                      SELECT inv.id,
                      ca.name AS cost_account_name,
                      p.name AS property_name,
                      inv.amount, inv.comment
                      FROM animals.invoice inv
                      JOIN animals.cost_account ca ON ca.id=inv.cost_account
                      JOIN animals.property p ON p.id=inv.property
                      WHERE inv.id=:invid
                      """, invid=i).first()
            d.append([self.view_link(text = i,
                                     route = 'invoice', item_id=i),
                      Text(q.cost_account_name),
                      Text(q.property_name),
                      Text(q.amount)])

        return dict( title='Done! Invoices created',
                     goto_url=self.request.route_url('invoice'),
                     goto_button_text='See all invoices',
                     data = d,
                     header = header )
