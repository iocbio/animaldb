# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound
from attrdict import AttrDict

from ..views.form import FormView
from ..views.text import *
from ..misc import *
from ..db import query
from ..error import GenericError

from .selector import Sel
from .cage_location import Cage
from .user import Permission, User
from .cost import CostAccount

@view_defaults(route_name='action_move_animals')
class ActionMoveAnimal(FormView):

    fields = [
        # order is used below in confirm view
        Field( 'cage_origin',
               title='Cage (Origin)', selector=Cage.refSelection),
        Field( 'comment', tags=tagText ),
        Field( 'user', title='User', view='user_name', selector=User.refSelection,
               comment='User who moved the animals',
               add_default = User.addDefault ),
        Field( 'cage_target',
               title='Cage (Target)', selector=Cage.refSelection),
    ]
        
    def __init__(self, request):
        FormView.__init__(self, request)
        self.cage_origin = self.request.params.get('cage_origin', '')
        if not self.cage_origin: self.cage_origin = None
        self.comment = self.request.params.get('comment', '')
        self.user = self.request.params.get('user', None)
        self.cage_target = self.request.params.get('cage_target', '')
        if not self.cage_target: self.cage_target = None

    @view_config(renderer='../templates/table_item_edit.jinja2')
    def cage_origin_entry(self):
        import copy
        
        # no cage or user selected, force selection
        if 'cage_origin' not in self.request.params or not self.user:
            return self.fill_form(fields = self.fields[0:3],
                                  title = 'Moving animals: Origin and User')

        co_name = self.value2text(Cage.refSelection, value=self.cage_origin) if self.cage_origin else 'None'
        fields = copy.deepcopy(self.fields)
        fields[0].readonly = True
        fields[0].title += ' - internal database ID'
        
        if self.cage_origin is None:
            qres = query(self.request,
                         """
                         SELECT n.id, n.name FROM animals.animal2cage_last am
                         JOIN animals.animal_name n ON n.id=am.animal
                         WHERE am.cage IS NULL ORDER BY n.id
                         """)
        else:
            qres = query(self.request,
                         """
                         SELECT n.id, n.name FROM animals.animal2cage_last am
                         JOIN animals.animal_name n ON n.id=am.animal
                         WHERE am.cage=:cage ORDER BY n.id
                         """, cage=self.cage_origin)
        for q in qres:
            fields.append( Field('animal_%d' % q.id, title='Animal: ' +  q.name,
                                 tags=tagBool) )
                       
        return self.fill_form(fields = fields,
                              title = 'Move animals from cage ' + co_name,
                              edit_url = self.request.current_route_url(_query={'confirm_form_submission': 1}))

    def process_input(self):
        if self.cage_origin==self.cage_target:
            raise GenericError(title='Animals not moved',
                               message='Animals do not move as origin and target cages are the same' )

    @view_config(renderer='../templates/table_item.jinja2', request_param='confirm_form_submission')
    def confirm(self):
        self.process_input()
        
        d = []
        names = {}
        for k in [ AttrDict( k='cage_origin', sql=Cage.refSelection ),
                   AttrDict( k='user', sql=User.refSelection ),
                   AttrDict( k='cage_target', sql=Cage.refSelection ) ]:
            n = self.value2text(k.sql, key=k.k) if self.request.params.get(k.k, '') else 'None'
            names[k.k] = n
            d.append(dict(text=Text(n),
                          field=Field(k.k)))
                
        d.append(dict(text = Text(self.comment), field=self.fields[1]))

        animids = []
        for k in self.request.params.keys():
            if k.find('animal_') == 0 and self.request.params[k]=='T':
                aid = k[len('animal_'):]
                animids.append(aid)
                q = query(self.request,
                          "SELECT n.id, n.name FROM animals.animal_name n WHERE n.id=:aid",
                          aid=aid).first()
                d.append(dict(text=self.view_link(text = q.name,
                                                  route = 'animal', item_id=aid),
                              field=Field(k, title='Animal')))

        uq = dict(cage_origin=self.cage_origin,
                  cage_target=self.cage_target,
                  user=self.user,
                  comment=self.comment,
                  perform_movement=self.session_store('move', animids))
        return dict( title='Confirm movement of %d animals from cage ' % len(animids) + names['cage_origin']
                     + " to cage " + names['cage_target'],
                     edit_url=self.request.current_route_url(_query=uq),
                     edit_button_text='Move animals',
                     data = d,
                     back_to_list = self.back_to_list )


    @view_config(request_param='perform_movement')
    def action(self):
        self.process_input()

        animals = self.session_load(key = self.request.params.get('perform_movement', '<>'),
                                    errormsg = 'Animal move expired',
                                    errorurl = True )
        for aid in animals:
            query(self.request,
                  """
                  INSERT INTO animals.animal2cage (animal, cage, "user", comment)
                  VALUES (:animal, :cage, :user, :comment)
                  """, animal=aid, cage=self.cage_target, user=self.user, comment=self.comment)

        url = self.request.current_route_url(_query=dict(report=self.session_store('report', animals),
                                                         cage_target = self.cage_target))
        raise HTTPFound(location = url)
                
    @view_config(renderer='../templates/animaldb_list.jinja2', request_param='report')
    def report(self):
        animals = self.session_load(key = self.request.params.get('report', '<>'),
                                    errormsg = 'Animal move report expired',
                                    errorurl = True )
        
        header = [Text('Animal')]
        d = []
        for aid in animals:
            q = query(self.request,
                      "SELECT n.id, n.name FROM animals.animal_name n WHERE n.id=:aid",
                      aid=aid).first()
            d.append([self.view_link(text = q.name,
                                     route = 'animal', item_id=aid)])

        cname = self.value2text(sql=Cage.refSelection, value=self.cage_target) if self.cage_target else 'None'
        res = self.view_with_selector('<>', 'animal', column='cage', value=self.cage_target)
        return dict( title='Done! Moved %d animals to cage %s' % (len(d), cname ),
                     goto_url=res.url,
                     goto_button_text='See all animals in cage ' + cname,
                     data = d,
                     header = header )
    
    @view_config(renderer='json', request_param=['field', 'query'])
    def field_selector(self):
        """Support for large selectors"""
        return FormView.field_selector(self)
