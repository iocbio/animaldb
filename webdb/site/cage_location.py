# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from ..views.table import TableView
from ..views.text import *
from ..misc import *

from .user import User

@view_defaults(route_name='location')
class Location(TableView):
    sql = """
SELECT loc.id, loc.name, COALESCE(counter.cages, 0) AS cages, loc.comment FROM animals.location loc
LEFT JOIN (SELECT lst.location, COUNT(*) AS cages from animals.cage2location_last lst
    GROUP BY lst.location) AS counter ON counter.location=loc.id
    """
    sql_table = "loc"

    db_schema = 'animals'
    db_table  = 'location'
    title = 'Locations'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name', title='Name' ),
        Field( 'cages', title='# Cages', readonly = True,
               process=lambda self, r: self.view_with_selector(text = r.cages, route = 'cage',
                                                               column = 'location', value=r.id) ),
        Field( 'comment', tags=tagText )
    ]

    refSelection = "SELECT loc.id AS value, loc.name AS text FROM animals.location loc ORDER BY loc.name"

    descendants = [
        Descendant( 'cage', 'location', 'Cages' ),
        Descendant( 'cage2location', 'location', 'Movement of cages into location' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='cage')
class Cage(TableView):
    sql = """
    SELECT c.id, c.name, c2l.location, loc.name AS location_name,
    c2l.when AS since, c2l.user, u.name AS user_name,
    CONCAT(COALESCE(counter_m.animals, '0'), ' males and ', COALESCE(counter_f.animals, '0'), ' females') AS animals,
    c.comment FROM animals.cage c
    LEFT JOIN animals.cage2location_last c2l ON c2l.cage = c.id
    LEFT JOIN animals.location loc ON c2l.location = loc.id
    LEFT JOIN animals.user u ON c2l.user = u.id
    LEFT JOIN (
      SELECT lst.cage, COUNT(*) AS animals
      FROM animals.animal2cage_last lst
      JOIN animals.animal am ON lst.animal=am.id
      WHERE am.sex='M'
      GROUP BY lst.cage) AS counter_m ON counter_m.cage=c.id
    LEFT JOIN (
      SELECT lst.cage, COUNT(*) AS animals
      FROM animals.animal2cage_last lst
      JOIN animals.animal am ON lst.animal=am.id
      WHERE am.sex='F'
      GROUP BY lst.cage) AS counter_f ON counter_f.cage=c.id
    """
    sql_table = "c"

    db_schema = 'animals'
    db_table  = 'cage'
    title = 'Cages'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name', title='Name' ),
        Field( 'location', title='Location', readonly = True, view = 'location_name',
               process=lambda self, r: self.view_link(text = r.location_name, route = 'location',
                                                      item_id=r.location) ),
        Field( 'since', title='Since', readonly = True ),
        Field( 'user', view='user_name', title = 'Moved by', readonly = True,
               process=lambda self, r: self.view_link(text = r.user_name, route = 'user', item_id=r.user) ),
        Field( 'animals', title='# Animals', readonly = True,
               process=lambda self, r: self.view_with_selector(text = r.animals, route = 'animal',
                                                               column = 'cage', value=r.id) ),
        Field( 'comment', tags=tagText )
    ]
    
    refProcess = lambda self, r: self.view_link(text = r.cage_name, route = 'cage', item_id=r.cage)
    refSelection = "SELECT c.id AS value, c.name AS text FROM animals.cage c ORDER BY c.name"

    descendants = [
        Descendant( 'animal', 'cage', 'Animals' ),
        Descendant( 'animal2cage', 'cage', 'Animal movements into cage' ),
        Descendant( 'cage2location', 'cage', 'Cage movements' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='cage2location')
class Cage2Location(TableView):
    sql = """
SELECT c2l.id,
    c2l.cage, c.name AS cage_name,
    c2l.location, loc.name AS location_name,
    c2l_last.id IS NOT NULL AS current,
    c2l.when,
    c2l.user, u.name AS user_name,
    c.comment
    FROM animals.cage2location c2l
    JOIN animals.cage c ON c.id = c2l.cage
    JOIN animals.location loc ON loc.id = c2l.location
    JOIN animals.user u ON u.id = c2l.user
    LEFT JOIN animals.cage2location_last c2l_last ON c2l_last.id=c2l.id
    """
    sql_table = "c2l"

    db_schema = 'animals'
    db_table  = 'cage2location'
    title = 'Cage movements'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'cage', title='Cage', view='cage_name', selector=Cage.refSelection, process=Cage.refProcess ),
        Field( 'location', title='Location', view='location_name', selector=Location.refSelection,
               process=lambda self, r: self.view_link(text = r.location_name, route = 'location', item_id=r.location) ),
        Field( 'current', title='Current location', tags=tagBool, readonly = True ),
        Field( 'when', title='When', tags=tagDateTime ),
        Field( 'user', title='User', view='user_name', selector=User.refSelection,
               comment='User who moved the cage into location',
               add_default = User.addDefault,
               process=lambda self, r: self.view_link(text = r.user_name, route = 'user', item_id=r.user) ),
        Field( 'comment', tags=tagText )
    ]

    @view_config()
    def view(self): return TableView.view(self)

