# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from ..views.table import TableView
from ..views.text import *
from ..misc import *

from .user import Permission
from .selector import Sel


@view_defaults(route_name='experiment_type')
class ExperimentType(TableView):
    sql = """
SELECT e.id, e.name, e.comment FROM animals.experiment_type e
    """
    sql_table = "e"

    db_schema = 'animals'
    db_table  = 'experiment_type'
    title = 'Experiment Types'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name', title='Name' ),
        Field( 'comment', tags=tagText )
    ]

    refSelection = "SELECT e.id AS value, e.name AS text FROM animals.experiment_type e ORDER BY e.name"

    descendants = [
        Descendant( 'experiment', 'experiment', 'Experiments' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='experiment')
class Experiment(TableView):
    sql = """
    SELECT e.id,
    e.animal, am.name AS animal_name,
    e.experiment_type, et.name AS experiment_type_name,
    e.permission, p.name AS permission_name,
    e.from, e.till,
    e.comment
    FROM animals.experiment e
    JOIN animals.animal_name am ON am.id = e.animal
    JOIN animals.experiment_type et ON et.id=e.experiment_type
    JOIN animals.permission p ON p.id = e.permission
    """
    sql_table = "e"

    db_schema = 'animals'
    db_table  = 'experiment'
    title = 'Experiments'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'animal', view = 'animal_name',
               large_selection = True, selector = Sel.animalSelect,
               process = lambda self, r: self.view_link(text = r.animal_name,
                                                        route = 'animal', item_id=r.animal) ),
        Field( 'experiment_type', view='experiment_type_name',
               selector=ExperimentType.refSelection,
               process = lambda self, r: self.view_link(text = r.experiment_type_name,
                                                        route = 'experiment_type', item_id=r.experiment_type) ),
        Field( 'permission', view = 'permission_name',
               selector = Permission.refSelection,
               process = lambda self, r: self.view_link(text = r.permission_name,
                                                        route = 'permission', item_id=r.permission) ),
        Field( 'from', tags=tagDateTime ),
        Field( 'till', tags=tagDateTime ),        
        Field( 'comment', tags=tagText )
    ]

    @view_config()
    def view(self): return TableView.view(self)
