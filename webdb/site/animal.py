# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from ..views.table import TableView
from ..views.text import *
from ..misc import *

from .ending import DeathReason, Export
from .user import Permission, User
from .selector import Sel
from .cage_location import Cage

@view_defaults(route_name='animal')
class Animal(TableView):
    sql = """
    SELECT am.id, sex, am2c.cage, c.name AS cage_name,
    litter, aml.birth,
    COALESCE(aif.in_facility, FALSE) AS in_facility,
    EXTRACT (epoch FROM
     CASE
     WHEN death IS NOT NULL THEN age(death,aml.birth)
     WHEN ex.departure IS NOT NULL THEN age(ex.departure,aml.birth)
     ELSE age(aml.birth)
     end)/24/60/60::integer AS age,
    death, death_reason, dr.name AS death_reason_name,
    death_permission, dp.name AS death_permission_name,
    export, ex.name AS export_name,
    COALESCE(par.itis, FALSE) as parent,
    matings.matings, offspring.offsprings,
    ear.earmark, props.properties,
    acl.cost_account, ca.name AS cost_account_name,
    am.comment
    FROM animals.animal am
    LEFT JOIN animals.litter aml ON am.litter = aml.id
    LEFT JOIN animals.animal2cage_last am2c ON am2c.animal = am.id
    LEFT JOIN animals.cage c ON c.id = am2c.cage
    LEFT JOIN animals.death_reason dr ON dr.id = am.death_reason
    LEFT JOIN animals.permission dp ON dp.id = am.death_permission
    LEFT JOIN animals.export ex ON ex.id = am.export
    LEFT JOIN (SELECT TRUE AS in_facility, id FROM animals.animal_in_facility) aif ON aif.id=am.id
    LEFT JOIN (SELECT TRUE AS itis, parent FROM animals.mating_parent GROUP BY parent) par ON par.parent=am.id
    LEFT JOIN
    (SELECT COUNT(*) AS matings, parent
    FROM (SELECT DISTINCT mating, parent FROM animals.mating_parent) p
    GROUP BY parent) matings ON matings.parent=am.id
    LEFT JOIN
    (SELECT COUNT(*) AS offsprings, parent
    FROM (SELECT DISTINCT mating, parent FROM animals.mating_parent) mp
    JOIN animals.mating mt ON mt.id = mp.mating
    JOIN animals.litter l ON l.mating = mt.id
    JOIN animals.animal am ON am.litter = l.id
    GROUP BY parent) offspring ON offspring.parent=am.id
    LEFT JOIN animals.earmark ear ON ear.animal=am.id
    LEFT JOIN animals.animal_property_array props ON props.animal = am.id
    LEFT JOIN animals.animal_cost_last acl ON acl.animal=am.id
    LEFT JOIN animals.cost_account ca ON ca.id=acl.cost_account
    """
    sql_table = "am" # short name of the main table in sql statement

    db_schema = 'animals'
    db_table  = 'animal'
    title = 'Animals'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'sex', selector = selMF ),
        Field( 'cage', title='Cage', view='cage_name',
               selector=Cage.refSelection, process=Cage.refProcess,
               readonly=True ),
        Field( 'litter',
               selector=Sel.litterSelect, tags=tagInt,
               large_selection = True,
               process=lambda self, r: self.view_link(text = r.litter, route = 'litter', item_id=r.litter) ),
        Field( 'birth', tags=tagDate, readonly=True ),
        Field( 'in_facility', title='Alive', comment='Animal is alive and in facility',
               tags=tagBool, readonly=True ),
        Field( 'age', unit='days', tags=tagInt, readonly=True ),
        Field( 'death', tags=tagDate ),
        Field( 'death_reason', view = 'death_reason_name', tags=tagInt,
               selector = DeathReason.refSelection ),
        Field( 'death_permission', view = 'death_permission_name',
               selector = Permission.refSelection ),
        Field( 'export', view = 'export_name',
               selector = Export.refSelection ),
        Field( 'parent', tags=tagBool, readonly = True, comment='Was it used as a parent' ),
        Field( 'matings', readonly = True ),
        Field( 'offsprings', readonly = True ),
        Field( 'properties', readonly = True, array = True,
               process = lambda self, r: Text(text=r.properties, css_class='tag-small') ),
        Field( 'earmark', readonly = True ),
        Field( 'cost_account', title='Cost account', view='cost_account_name', readonly = True,
               process=lambda self, r: self.view_link(text = r.cost_account_name,
                                                      route = 'cost_account', item_id=r.cost_account) ),
        Field( 'comment', tags=tagText )
    ]

    descendants = [
        Descendant( 'animal_property', 'animal', 'Properties/Tags' ),
        Descendant( 'earmark', 'animal', 'Earmark' ),
        Descendant( 'mating_parent', 'parent', 'As parent' ),
        Descendant( 'animal2cage', 'animal', 'Movements between cages' ),
        Descendant( 'experiment', 'animal', 'Experiments on animal' ),
        Descendant( 'action_rm_animal', 'animal', 'Take out/Export', is_action=True ),
        Descendant( 'animal_cost', 'animal', 'Animal cost' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='earmark')
class Earmark(TableView):
    sql = """
    SELECT ear.id,
    ear.animal, am.name AS animal_name,
    ear.earmark
    FROM animals.earmark ear
    JOIN animals.animal_name am ON am.id = ear.animal
    """
    sql_table = "ear"

    db_schema = 'animals'
    db_table  = 'earmark'
    title = 'Earmarks'

    can_remove = True

    fields = [
        Field( 'id', title='ID' ),
        Field( 'animal', view = 'animal_name',
               large_selection = True, selector = Sel.animalSelect,
               process = lambda self, r: self.view_link(text = r.animal_name,
                                                        route = 'animal', item_id=r.animal) ),
        Field( 'earmark', tags=tagInt )
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='property')
class Property(TableView):
    sql = """
    SELECT p.id, p.name, p.inherited, p.cost, p.sorting_order,
    counter.animals, p.comment
    FROM animals.property p
    LEFT JOIN
    (SELECT COUNT(*) AS animals, ap.property
     FROM animals.animal_property ap
     JOIN animals.animal am ON am.id=ap.animal
     WHERE (am.death IS NULL OR am.death > now()) AND (am.export IS NULL)
     GROUP BY ap.property) counter ON counter.property=p.id
    """
    sql_table = 'p'

    db_schema = 'animals'
    db_table  = 'property'
    title = 'Description of Animal Properties/Tags'

    can_remove = True

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name' ),
        Field( 'inherited', tags=tagBool,
               comment='Whether the animal property is inherited by descendants' ),
        Field( 'cost', title='Cost/Day', tags=tagBool,
               comment='Is the property associated with the cost per day' ),
        Field( 'sorting_order', title='Sorting order', tags=tagInt,
               comment='Allows to group the same types of properties by setting the same sorting order'),
        Field( 'animals', title='# Animals',
               comment='Number of animals with this property alive now and in the facility', readonly=True ),
        Field( 'comment', tags=tagText )
    ]

    refSelection = """
    SELECT id AS value, name AS text FROM animals.property
    ORDER BY sorting_order, name
    """

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='animal_property')
class AnimalProperty(TableView):
    sql = """
    SELECT ap.id,
    ap.animal, CONCAT(am.id::text, ' / ', am.sex) AS animal_name,
    ap.property, p.name AS property_name
    FROM animals.animal_property ap
    JOIN animals.animal am ON am.id = ap.animal
    JOIN animals.property p ON p.id = ap.property
    """
    sql_table = 'ap'

    db_schema = 'animals'
    db_table  = 'animal_property'
    title = 'Animal Properties/Tags'

    can_remove = True
    can_remove_simple = True

    fields = [
        Field( 'id', title='ID' ),
        Field( 'animal', view = 'animal_name',
               large_selection = True, selector = Sel.animalSelect,
               process = lambda self, r: self.view_link(text = r.animal_name,
                                                        route = 'animal', item_id=r.animal) ),
        Field( 'property', view='property_name',
               selector = Property.refSelection,
               process = lambda self, r: self.view_link(text = r.property_name, css_class='tag-small',
                                                        route = 'property', item_id=r['property']) ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='animal2cage')
class Animal2Cage(TableView):
    sql = """
    SELECT a2c.id,
    a2c.animal, am.name AS animal_name,
    a2c.cage, c.name AS cage_name,
    a2c_last.id IS NOT NULL AS current,
    a2c.when,
    a2c.user, u.name AS user_name,
    c.comment
    FROM animals.animal2cage a2c
    JOIN animals.animal_name am ON am.id = a2c.animal
    LEFT JOIN animals.cage c ON c.id = a2c.cage
    JOIN animals.user u ON u.id = a2c.user
    LEFT JOIN animals.animal2cage_last a2c_last ON a2c_last.id=a2c.id
    """
    sql_table = "a2c"

    db_schema = 'animals'
    db_table  = 'animal2cage'
    title = 'Animal movements'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'animal', view = 'animal_name',
               large_selection = True, selector = Sel.animalSelect,
               process = lambda self, r: self.view_link(text = r.animal_name,
                                                        route = 'animal', item_id=r.animal) ),
        Field( 'cage', title='Cage', view='cage_name', tags=tagInt,
               selector=Cage.refSelection, process=Cage.refProcess ),
        Field( 'current', title='Current location', tags=tagBool, readonly = True ),
        Field( 'when', title='When', tags=tagDateTime ),
        Field( 'user', title='User', view='user_name', selector=User.refSelection,
               comment='User who moved the animal into the cage',
               add_default = User.addDefault,
               process=lambda self, r: self.view_link(text = r.user_name, route = 'user', item_id=r.user) ),
        Field( 'comment', tags=tagText )
    ]

    @view_config()
    def view(self): return TableView.view(self)
