# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

# Collection of selectors and process functions. Had to put them
# separately to avoid cyclic dependencies

class Sel:

    # Animal
    animalSelect = """
    SELECT am.id AS value, am.name AS text
    FROM animals.animal_name am
    ORDER BY am.id
    """

    # Litter
    litterSelect = """
    SELECT id AS value, id::text || ' / ' || birth::text AS text FROM animals.litter ORDER BY id
    """
    
    # Mating
