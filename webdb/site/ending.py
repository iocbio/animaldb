# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from ..views.table import TableView
from ..views.text import *
from ..misc import *

@view_defaults(route_name='death_reason')
class DeathReason(TableView):
    sql = """
    SELECT dr.id, dr.name, dr.description
    FROM animals.death_reason dr
"""
    sql_table = "dr" # short name of the main table in sql statement

    db_schema = 'animals'
    db_table  = 'death_reason'
    title = 'Death Reasons'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name' ),
        Field( 'description', tags=tagText )
    ]

    refSelection = """
    SELECT id AS value, name AS text FROM animals.death_reason
    ORDER BY name
    """

    descendants = [
        Descendant( 'animal', 'death_reason', 'Animal endings' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='export')
class Export(TableView):
    sql = """
    SELECT ex.id, ex.name, ex.departure, ex.comment
    FROM animals.export ex
"""
    sql_table = "ex" # short name of the main table in sql statement

    db_schema = 'animals'
    db_table  = 'export'
    title = 'Exports'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name' ),
        Field( 'depature', tags=tagDate ),
        Field( 'comment', tags=tagText )
    ]

    refSelection = """
    SELECT id AS value, name AS text FROM animals.export
    ORDER BY name
    """

    descendants = [
        Descendant( 'animal', 'export', 'Exported animals' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)
