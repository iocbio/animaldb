# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2019-2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from ..views.table import TableView
from ..views.text import *
from ..misc import *

@view_defaults(route_name='user')
class User(TableView):
    sql = """
SELECT u.id, u.name, u.database_user_name, u.phone, u.email FROM animals.user u
    """
    sql_table = "u"

    db_schema = 'animals'
    db_table  = 'user'
    title = 'Users'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name', title='Name' ),
        Field( 'database_user_name', title='Database user', comment='Username in the database, if available' ),
        Field( 'phone', title='Phone' ),
        Field( 'email', title='E-Mail' )
    ]

    refSelection = "SELECT u.id AS value, u.name AS text FROM animals.user u ORDER BY u.name"

    descendants = [
        Descendant( 'permission', 'user', 'Permissions' ),
    ]

    def addDefault(request):
        from ..db import query
        for q in query(request,
                       'SELECT id AS uid FROM animals.user WHERE database_user_name=:user',
                       user = request.authenticated_userid):
            return q.uid
        return ''

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='permission')
class Permission(TableView):
    sql = """
SELECT p.id, p.name, p.user, u.name AS user_name, p.from, p.till, p.comment
    FROM animals.permission p
    JOIN animals.user u ON u.id = p.user
    """
    sql_table = "p"

    db_schema = 'animals'
    db_table  = 'permission'
    title = 'Permissions'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'name', title='Name' ),
        Field( 'user', title='User', view='user_name', selector=User.refSelection,
               process=lambda self, r: self.view_link(text = r.user_name, route = 'user', item_id=r.user) ),
        Field( 'from', tags=tagDate ),
        Field( 'till', tags=tagDate ),
        Field( 'comment', tags=tagText )
    ]

    refSelection = """
    SELECT p.id AS value, CONCAT(p.name, ' / ', u.name, ' / ', p.from::text, '-', p.till::text) AS text
    FROM animals.permission p
    JOIN animals.user u on p.user=u.id
    ORDER BY p.name, u.name, p.from, p.till
    """

    descendants = [
        Descendant( 'animal', 'death_permission', 'Animals' ),
        Descendant( 'mating', 'death_permission', 'Matings' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)


