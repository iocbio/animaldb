# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO WebDB
#

class R:
    def __init__(self, route, title, visible=True):
        self.route = route
        self.title = title
        self.visible = visible

# Dictionary used to fill the main page
# Using the notation:
#  Subtitle <dictionary key>:
#     list of elements with page url and title
Routing = {
    'Animals': [
        R ('animal', 'Animals' ),
        R ('earmark', 'Earmarks', False),
        R ('property', 'Description of Animal Properties/Tags' ),
        R ('animal_property', 'Animal properties', False ),
        R ('animal2cage', 'Movement of animals' ),
        R ('action_move_animals', 'Move group of animals between cages'),
        R ('action_rm_animal', 'Take out or remove animals', False),
        R ('experiment', 'Experiments on animals' ),        
    ],
    
    'Matings and Imports/Exports': [
        R( 'litter', 'Litters' ),
        R( 'action_add_animals', 'Add animals to litter' ),
        R( 'mating', 'Matings' ),
        R( 'mating_parent', 'Matings: Parents' ),
        R( 'import', 'Imports' ),
        R( 'export', 'Exports' ),
    ],

    'Cages and Locations': [
        R( 'cage', 'Cages' ),
        R( 'location', 'Locations' ),
        R( 'cage2location', 'Movement of cages', False ),
    ],

    'Users and Legal' : [
        R( 'user', 'Users' ),
        R( 'permission', 'Permissions' ),
        R( 'death_reason', 'Death reasons' ),
        R( 'experiment_type', 'Experiment Types' ),
    ],

    'Costs' : [
        R( 'cost_account', 'Cost Accounts' ),
        R( 'animal_cost', 'Animal Costs' ),
        R( 'invoice', 'Invoices' ),
        R( 'cost_account_costs_next_invoice', 'Costs accumulated since last invoice' ),
        R( 'cost_account_costs_accumulated', 'Costs accumulated over all time' ),
        R( 'action_new_invoices', 'Create new invoices' ),
    ],
}

# define routes used in the application
def routes(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('error', '/error')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')

    for r in Routing.values():
        for i in r:
            config.add_route(i.route, '/' + i.route)
