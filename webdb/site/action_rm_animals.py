# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound
from attrdict import AttrDict

from ..views.form import FormView
from ..views.text import *
from ..misc import *
from ..db import query
from ..error import GenericError

from .selector import Sel
from .user import Permission, User
from .ending import DeathReason, Export

import dateutil.parser

@view_defaults(route_name='action_rm_animal')
class ActionRmAnimal(FormView):

    fields = [
        # order is used below in confirm view
        Field( 'animal', 
               large_selection = True, selector = Sel.animalSelect,
               process = lambda self, r: self.view_link(text = r.animal_name,
                                                        route = 'animal', item_id=r.animal) ),
        Field( 'user', title='User', view='user_name', selector=User.refSelection,
               comment='User who moved the animal out of the cage',
               add_default = User.addDefault )
    ]
        
    def __init__(self, request):
        FormView.__init__(self, request)
        self.animal = self.request.params.get('animal', '')
        self.death_reason = self.request.params.get('death_reason', '')
        self.death_permission = self.request.params.get('death_permission', '')
        self.export = self.request.params.get('export', '')
        self.user = self.request.params.get('user', None)
        # as death is a date
        if 'death_date_submit' in self.request.params:
            self.death = dateutil.parser.isoparse(self.request.params['death_date_submit']).date().isoformat()
        else:
            self.death = self.request.params.get('death', '')

    @view_config(renderer='../templates/table_item_edit.jinja2')
    def entry(self):
        import copy
        
        # no animal selected, force selection
        if not self.animal:
            return self.fill_form(fields = self.fields[0:1],
                                  title = 'Taking out or exporting animal: Select animal')

        fields = copy.copy(self.fields)

        def proc(a):
            return str(a) if a is not None else ''

        d_dt, d_permission, d_reason, exp = '', '', '', ''            
        for q in query(self.request,
                       """
                       SELECT death, death_permission, death_reason, export
                       FROM animals.animal a
                       WHERE a.id=:animal
                       """, animal=self.animal):
            d_dt = q.death if q.death else ''
            d_permission = proc(q.death_permission)
            d_reason = proc(q.death_reason)
            exp = proc(q.export)

        fields.extend([
            Field( 'death', tags=tagDate, add_default = d_dt ),
            Field( 'death_reason', tags=tagInt,add_default = d_reason,
               selector = DeathReason.refSelection ),
            Field( 'death_permission', add_default = d_permission,
                   selector = Permission.refSelection ),
            Field( 'export', add_default = exp,
                   selector = Export.refSelection )
        ])

        uq = {}
        uq['perform_removal']=self.session_store('removal', 1)
        return self.fill_form(fields = fields,
                              title = 'Taking out or exporting animal',
                              edit_url = self.request.current_route_url(_query=uq))


    @view_config(renderer='../templates/animaldb_list.jinja2', request_param='perform_removal')
    def action(self):
        # check if action has been used already
        self.session_load(key = self.request.params.get('perform_removal', '<>'),
                          errormsg = 'Taking out or export of the animal expired',
                          errorurl = True )

        if not self.user:
            raise GenericError(title='Error while taking out or exporting animals',
                               message= 'Error: You have not specified user moving the animal')
            
        if not (self.death and self.death_permission and self.death_reason) and not self.export:
            raise GenericError(title='Error while taking out or exporting animals',
                               message= \
                               'Error: You have not specified all death fields (if taking out) nor ' +
                               'export field (if exporting from facility)')
        if self.export:
            query(self.request,
                  "UPDATE animals.animal SET export=:exp WHERE animal.id=:id",
                  id = self.animal, exp = self.export)
        else:
            query(self.request,
                  """UPDATE animals.animal SET death=:death,death_reason=:reason,death_permission=:permission
                  WHERE animal.id=:id""",
                  id = self.animal, death = self.death, reason=self.death_reason,
                  permission=self.death_permission)
            
        query(self.request,
              """INSERT INTO animals.animal2cage (animal, cage, "user") VALUES (:animal, NULL, :user)""",
              animal=self.animal, user=self.user)

        url = self.request.route_url(route_name='animal',
                                     _query=dict(item_id = self.animal, view=1))
        raise HTTPFound(location = url)

    @view_config(renderer='json', request_param=['field', 'query'])
    def field_selector(self):
        """Support for large selectors"""
        return FormView.field_selector(self)
