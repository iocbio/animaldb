# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2020
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  This file is part of project: IOCBIO AnimalsDB
#

from pyramid.view import view_config, view_defaults
from ..views.table import TableView
from ..views.text import *
from ..misc import *

from .selector import Sel

@view_defaults(route_name='mating')
class Mating(TableView):
    sql = """
SELECT m.id, m.success,
    CONCAT(mo.from, '-') AS from,
    CONCAT(mo.till, '-') AS till,
    CONCAT(mo.male_parents, '(M) ', mo.female_parents, '(F)') AS parents, 
    COALESCE(lit.litters, 0) AS litters,
    CONCAT(COALESCE(off_m.offsprings, '0'), ' males and ', COALESCE(off_f.offsprings, '0'), ' females') AS offsprings,
    mio.properties, mio.properties_names,
    mio.death_permission, per.name AS death_permission_name,
    mio.cost_account, ca.name AS cost_account_name,
    m.comment
    FROM animals.mating m
    JOIN animals.mating_overview mo ON mo.id = m.id
    LEFT JOIN (SELECT mating, COUNT(*) AS litters FROM animals.litter GROUP BY mating) lit ON lit.mating=m.id
    LEFT JOIN 
    (SELECT COUNT(*) as offsprings, lit.mating
    FROM animals.litter lit
    JOIN animals.animal am ON lit.id=am.litter
    WHERE am.sex='M'
    GROUP BY lit.mating) off_m ON off_m.mating=m.id
    LEFT JOIN 
    (SELECT COUNT(*) as offsprings, lit.mating
    FROM animals.litter lit
    JOIN animals.animal am ON lit.id=am.litter
    WHERE am.sex='F'
    GROUP BY lit.mating) off_f ON off_f.mating=m.id
    LEFT JOIN animals.mating_inherited_overview mio ON mio.mating=m.id
    LEFT JOIN animals.cost_account ca ON ca.id=mio.cost_account
    LEFT JOIN animals.permission per ON per.id = mio.death_permission
    """
    sql_table = "m"

    db_schema = 'animals'
    db_table  = 'mating'
    title = 'Matings'

    fields = [
        Field( 'id', title='ID' ),
        Field( 'from', title='From', readonly = True ),
        Field( 'till', title='Till', readonly = True ),
        Field( 'success', title='Success', tags=tagBool ),
        Field( 'parents', title='Parents', readonly = True ),
        Field( 'litters', title='# Litters', readonly = True ),
        Field( 'offsprings', readonly = True ),
        Field( 'properties', view='properties_names', comment='Inherited properties of all parents',
               process = lambda self, r: Text(text=r.properties_names, css_class='tag-small'),
               readonly = True, array = True ),
        Field( 'cost_account', title='Cost account', view='cost_account_name', readonly = True,
               process=lambda self, r: self.view_link(text = r.cost_account_name,
                                                      route = 'cost_account', item_id=r.cost_account) ),
        Field( 'death_permission', view = 'death_permission_name',
               process=lambda self, r: self.view_link(text = r.death_permission_name,
                                                      route = 'permission', item_id=r.death_permission),
               readonly=True ),
        Field( 'comment', tags=tagText )
    ]

    refProcess = lambda self, r: self.view_link(text = r.mating, route = 'mating', item_id=r.mating)
    refSelection = """
    SELECT m.id AS value, CONCAT(m.id, ' / ', m.from, '-', m.till, ': ',
    m.male_parents, '(M) ', m.female_parents, '(F)') AS text
    FROM animals.mating_overview m
    ORDER BY m.from DESC, m.till DESC, m.id, m.male_parents, m.female_parents
    """

    descendants = [
        Descendant( 'mating_parent', 'mating', 'Parents' ),
        Descendant( 'litter', 'mating', 'Litters' ),
    ]

    @view_config()
    def view(self): return TableView.view(self)

    
@view_defaults(route_name='mating_parent')
class MatingParent(TableView):
    sql = """
SELECT p.id, p.mating,
    p.parent, am.name AS parent_name,
    p.from, p.till,
    p.comment
FROM animals.mating_parent p
LEFT JOIN animals.animal_name am on p.parent=am.id
"""
    sql_table = "p"
    db_schema = 'animals'
    db_table  = 'mating_parent'
    title = 'Matings: Parents'
    
    fields = [
        Field( 'id', title='ID' ),
        Field( 'mating', process = Mating.refProcess, large_selection = True, selector = Mating.refSelection ),
        Field( 'parent', view = 'parent_name',
               large_selection = True, selector = Sel.animalSelect,
               process = lambda self, r: self.view_link(text = r.parent_name,
                                                        route = 'animal', item_id=r.parent) ),
        Field( 'from', tags=tagDate ),
        Field( 'till', tags=tagDate ),
        Field( 'comment', tags=tagText )
    ]

    @view_config()
    def view(self): return TableView.view(self)


@view_defaults(route_name='import')
class Import(TableView):
    sql = """
    SELECT 
    imp.id, imp.name, imp.arrival,
    COALESCE(lit.litters, 0) AS litters,
    CONCAT(COALESCE(off_m.offsprings, '0'), ' males and ', COALESCE(off_f.offsprings, '0'), ' females') AS animals,
    imp.comment 
    FROM animals.import imp
    LEFT JOIN (SELECT import, COUNT(*) AS litters FROM animals.litter GROUP BY import) lit ON lit.import=imp.id
    LEFT JOIN 
    (SELECT COUNT(*) as offsprings, lit.import
    FROM animals.litter lit
    JOIN animals.animal am ON lit.id=am.litter
    WHERE am.sex='M'
    GROUP BY lit.import) off_m ON off_m.import=imp.id
    LEFT JOIN 
    (SELECT COUNT(*) as offsprings, lit.import
    FROM animals.litter lit
    JOIN animals.animal am ON lit.id=am.litter
    WHERE am.sex='F'
    GROUP BY lit.import) off_f ON off_f.import=imp.id
    """
    sql_table = "imp"
    db_schema = 'animals'
    db_table  = 'import'
    title = 'Imports'
    
    fields = [
        Field( 'id', title='ID' ),
        Field( 'name' ),
        Field( 'arrival', tags=tagDate ),
        Field( 'litters', title='# Litters', readonly = True ),
        Field( 'animals', title='# Imported animals', readonly = True ),
        Field( 'comment', tags=tagText )
    ]

    descendants = [
        Descendant( 'litter', 'import', 'Imported litters' )
    ]
    
    refSelection = """
    SELECT id AS value, CONCAT(arrival::text, ' / ', name) AS text
    FROM animals.import
    ORDER BY arrival DESC, name
    """

    @view_config()
    def view(self): return TableView.view(self)

    
@view_defaults(route_name='litter')
class Litter(TableView):
    sql = """
    SELECT 
    l.id, l.birth, 
    l.mating,
    CONCAT(mo.id, ' / ', mo.from, '-', mo.till, ': ', mo.male_parents, '(M) ',
           mo.female_parents, '(F)') as mating_name,
    l.import, imp."name" as import_name,
    CONCAT(COALESCE(off_m.offsprings, '0'), ' males and ', COALESCE(off_f.offsprings, '0'), ' females') AS offsprings,
    mio.properties, mio.properties_names,
    l."comment" 
    FROM animals.litter l
    LEFT JOIN animals.mating_overview mo ON l.mating = mo.id
    LEFT JOIN animals.import imp ON imp.id=l."import" 
    LEFT JOIN 
    (SELECT COUNT(*) as offsprings, am.litter
    FROM animals.litter lit
    JOIN animals.animal am ON lit.id=am.litter
    WHERE am.sex='M'
    GROUP BY am.litter) off_m ON off_m.litter=l.id
    LEFT JOIN 
    (SELECT COUNT(*) as offsprings, am.litter
    FROM animals.litter lit
    JOIN animals.animal am ON lit.id=am.litter
    WHERE am.sex='F'
    GROUP BY am.litter) off_f ON off_f.litter=l.id
    LEFT JOIN animals.mating_inherited_overview mio ON mio.mating=l.mating
    """
    sql_table = "l"
    db_schema = 'animals'
    db_table  = 'litter'
    title = 'Litters'
    
    fields = [
        Field( 'id', title='ID' ),
        Field( 'birth', tags=tagDate ),
        Field( 'mating', view='mating_name', 
               process = Mating.refProcess, large_selection = True, selector = Mating.refSelection ),
        Field( 'import', view='import_name', selector = Import.refSelection,
               process = lambda self, r: self.view_link(text=r.import_name, route='import', item_id=r['import'])),
        Field( 'offsprings', title='Offsprings', readonly = True ),
        Field( 'properties', view='properties_names', comment='Inherited properties of all parents',
               process = lambda self, r: Text(text=r.properties_names, css_class='tag-small'),
               readonly = True, array = True ),
        Field( 'comment', tags=tagText )
    ]

    descendants = [
        Descendant( 'animal', 'litter', 'Animals from this litter' ),
        Descendant( 'action_add_animals', 'litter', 'Add animals to this litter', is_action=True ),
    ]

    
    @view_config()
    def view(self): return TableView.view(self)


